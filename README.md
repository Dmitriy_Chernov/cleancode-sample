Clean code sample
=========================
This is a sample app using the Uncle Bob's clean architecture approach.

App content
-----------
This app can make a little yet: authorise you as a GitHub user and then show a repository list (in current version it's JakeWharton repositories)

App have three screens:
 
 1) splash (check of authorisation for definition of next screen)
 
 2) authorisation (include authorisation and registration (not finished, exists as navigation sample),
 
 3) github repository list (with support of pagination and offline mode).

In general, app works in offline mode and not afraid rotates of the device.

The most important, it's ready for changes and extensions! ;)

Tests
-----------
I tried to show how to build effective tests (local and instrumented unit tests, UI tests).
All app layers had covered by tests.

License
--------

    Copyright 2017 Dmitriy Chernov

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.