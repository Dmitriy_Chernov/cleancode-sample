package com.example.dchernov.cleancodesample.domain.interactors;

import com.example.dchernov.cleancodesample.data.db.converters.RepositoryConverter;
import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.data.net.GithubApi;
import com.example.dchernov.cleancodesample.data.net.models.responses.RepositoryResponse;
import com.example.dchernov.cleancodesample.data.repositories.ReposRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulersTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author dmitriy
 */
public class GetRepositoriesInteractorTest {


    @Mock private ReposRepository reposRepository;
    @Mock private GithubApi githubApi;
    @Mock private PageLoader<List<Repository>> pageLoader;

    private GetRepositoriesInteractor getRepositoriesInteractor;
    private RxSchedulers rxSchedulers = new RxSchedulersTest();
    private RepositoryConverter repositoryConverter = new RepositoryConverter();

    private String userName = "JakeWharton";
    private int pageSize = GithubApi.PAGE_SIZE;

    private RepositoryResponse repositoryResponseSample = new RepositoryResponse();

    private List<RepositoryResponse> fullPage =
            Collections.nCopies(pageSize, repositoryResponseSample);

    private List<RepositoryResponse> lastPage =
            Collections.singletonList(repositoryResponseSample);

    @Before
    public void beforeEachTest() {

        MockitoAnnotations.initMocks(this);

        getRepositoriesInteractor = new GetRepositoriesInteractorImpl(
                reposRepository, githubApi, rxSchedulers, repositoryConverter, userName, pageSize
        );
    }

    @Test
    public void getTwoPages_success() {

        List<Repository> localRepositories =
                Collections.singletonList(new Repository(1, "localRepository"));

        //mock api
        when(githubApi.getRepositoriesPage(userName, 1, pageSize))
                .thenReturn(Single.fromCallable(() -> fullPage));
        when(githubApi.getRepositoriesPage(userName, 2, pageSize))
                .thenReturn(Single.fromCallable(() -> lastPage));

        //mock reposRepository
        when(reposRepository.getRepositoryList())
                .thenReturn(Single.fromCallable(() -> localRepositories));
        when(reposRepository.deleteRepositories())
                .thenReturn(Completable.complete());
        when(reposRepository.insertRepositoryList(anyList()))
                .thenReturn(Single.fromCallable(() -> Collections.singletonList(1L)));

        InOrder inOrder = inOrder(pageLoader);

        getRepositoriesInteractor.setPageLoader(pageLoader);
        getRepositoriesInteractor.refresh();

        //get local data
        inOrder.verify(pageLoader).onClear();
        inOrder.verify(pageLoader).onGetNextPage(anyList());

        //get first page
        inOrder.verify(pageLoader).onClear();
        inOrder.verify(pageLoader).showPageProgress();
        inOrder.verify(pageLoader).onGetNextPage(anyList());

        //request next page (handled by user, in scrolling for example)
        getRepositoriesInteractor.loadNextPage();

        //get second page and stop pagination
        inOrder.verify(pageLoader).onGetNextPage(anyList());
        inOrder.verify(pageLoader).hidePageProgress();

        //try scroll and check that next page request not called
        getRepositoriesInteractor.loadNextPage();
        verify(pageLoader, atLeast(2)).onGetNextPage(anyList());
    }

    @Test
    public void getFirstPage_Failure() {

        List<Repository> localRepositories =
                Collections.singletonList(new Repository(1, "localRepository"));

        //mock api (simulation of network error)
        when(githubApi.getRepositoriesPage(userName, 1, pageSize))
                .thenReturn(Single.error(UnknownHostException::new));
        //mock reposRepository
        when(reposRepository.getRepositoryList())
                .thenReturn(Single.fromCallable(() -> localRepositories));

        InOrder inOrder = inOrder(pageLoader);

        getRepositoriesInteractor.setPageLoader(pageLoader);
        getRepositoriesInteractor.refresh();

        //get local data
        inOrder.verify(pageLoader).onClear();
        inOrder.verify(pageLoader).onGetNextPage(anyList());

        //throwable on get first page
        inOrder.verify(pageLoader).onThrowable(any());

    }

    @Test
    public void getEmptyLocalListAndSinglePage_success() throws Exception {

        List<Repository> localRepositories = Collections.emptyList();

        //mock api
        when(githubApi.getRepositoriesPage(userName, 1, pageSize))
                .thenReturn(Single.fromCallable(() -> lastPage));
        //mock reposRepository
        when(reposRepository.getRepositoryList())
                .thenReturn(Single.fromCallable(() -> localRepositories));
        when(reposRepository.deleteRepositories())
                .thenReturn(Completable.complete());
        when(reposRepository.insertRepositoryList(anyList()))
                .thenReturn(Single.fromCallable(() -> Collections.singletonList(1L)));

        InOrder inOrder = inOrder(pageLoader);

        getRepositoriesInteractor.setPageLoader(pageLoader);
        getRepositoriesInteractor.refresh();

        //get single page, show and hide pagination
        inOrder.verify(pageLoader).onClear();
        inOrder.verify(pageLoader).showPageProgress();
        inOrder.verify(pageLoader).onGetNextPage(anyList());
        inOrder.verify(pageLoader).hidePageProgress();

        verify(pageLoader, never()).onThrowable(any());

    }

}
