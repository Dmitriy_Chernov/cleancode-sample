package com.example.dchernov.cleancodesample.presentation.auth.presenter;

import com.example.dchernov.cleancodesample.presentation.auth.navigation.AuthRouter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * @author dchernov
 */
public class AuthPresenterTest {

    @Mock private AuthRouter authRouter;

    private AuthPresenterImpl authPresenter;

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);

        authPresenter = new AuthPresenterImpl(authRouter);
    }

    @Test
    public void authRouterMoveToSignIn_success() {
        authPresenter.onFirstViewAttach();

        verify(authRouter).moveToSignIn();
    }

}
