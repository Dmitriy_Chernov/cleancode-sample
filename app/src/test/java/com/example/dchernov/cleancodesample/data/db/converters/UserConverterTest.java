package com.example.dchernov.cleancodesample.data.db.converters;

import com.example.dchernov.cleancodesample.data.db.models.User;
import com.example.dchernov.cleancodesample.data.net.models.responses.UserResponse;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author dchernov
 */
public class UserConverterTest {

    private UserConverter userConverter;

    @Before
    public void beforeEachTest() {
        userConverter = new UserConverter();
    }

    @Test
    public void responseConvert_test() {

        //init response
        UserResponse userResponse = new UserResponse(42, "sample_login", "sample_avatarUrl");

        //get user
        User user = userConverter.convert(userResponse);

        //check user
        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo(userResponse.getId());
        assertThat(user.getLogin()).isEqualTo(userResponse.getLogin());
    }

}
