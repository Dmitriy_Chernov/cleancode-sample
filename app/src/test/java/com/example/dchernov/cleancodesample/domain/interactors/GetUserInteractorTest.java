package com.example.dchernov.cleancodesample.domain.interactors;

import com.example.dchernov.cleancodesample.data.db.models.User;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulersTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author dchernov
 */
public class GetUserInteractorTest {

    @Mock private UserRepository userRepository;

    private RxSchedulers rxSchedulers = new RxSchedulersTest();

    private GetUserInteractor getUserInteractor;

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);

        getUserInteractor = new GetUserInteractorImpl(userRepository, rxSchedulers);
    }

    @Test
    public void getUser_success() {

        when(userRepository.getUser()).thenReturn(Single.fromCallable(() -> new User(1, "loginText")));

        TestObserver<User> testObserver = TestObserver.create();
        getUserInteractor.getUser().subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertComplete();
        testObserver.assertNoErrors();

        User user = testObserver.values().get(0);

        assertThat(user).isNotNull();
        assertThat(user.getId()).isNotZero();
        assertThat(user.getLogin()).isNotEmpty();
    }

    @Test
    public void getUser_null() {

        when(userRepository.getUser()).thenReturn(Single.fromCallable(() -> null));

        TestObserver<User> testObserver = TestObserver.create();
        getUserInteractor.getUser().subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNotComplete();
        testObserver.assertError(NullPointerException.class);
    }

    @Test
    public void getUser_failure() {

        when(userRepository.getUser()).thenReturn(Single.error(UnknownError::new));

        TestObserver<User> testObserver = TestObserver.create();
        getUserInteractor.getUser().subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNotComplete();
        testObserver.assertError(UnknownError.class);
    }

}
