package com.example.dchernov.cleancodesample.data.db.converters;

import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.data.net.models.responses.RepositoryResponse;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author dchernov
 */
public class RepositoryConverterTest {

    private RepositoryConverter repositoryConverter;

    @Before
    public void beforeEachTest() {
        repositoryConverter = new RepositoryConverter();
    }

    @Test
    public void responseConvert_test() {

        //init response
        RepositoryResponse repositoryResponse =
                new RepositoryResponse(42, "sample_name", "sample_desc", "sample_url");

        //get repository
        Repository repository = repositoryConverter.convert(repositoryResponse);

        //check repository
        assertThat(repository).isNotNull();
        assertThat(repository.getId()).isEqualTo(repositoryResponse.getId());
        assertThat(repository.getName()).isEqualTo(repositoryResponse.getName());
    }

    @Test
    public void responseListConvert_test() {

        //init response list
        RepositoryResponse repositoryResponse1 = new RepositoryResponse(1, "name1", "desc1", "url1");
        RepositoryResponse repositoryResponse2 = new RepositoryResponse(2, "name2", "desc2", "url2");
        List<RepositoryResponse> repositoryResponseList =
                Arrays.asList(repositoryResponse1, repositoryResponse2);

        //get repository list
        List<Repository> repositoryList = repositoryConverter.convert(repositoryResponseList);

        //check repository list
        assertThat(repositoryList).isNotNull();
        assertThat(repositoryList.size()).isEqualTo(repositoryResponseList.size());

        //check items

        //check item0
        Repository repository1 = repositoryList.get(0);
        assertThat(repository1).isNotNull();
        assertThat(repository1.getName()).isEqualTo(repositoryResponse1.getName());
        assertThat(repository1.getId()).isEqualTo(repositoryResponse1.getId());

        //check item1
        Repository repository2 = repositoryList.get(1);
        assertThat(repository2).isNotNull();
        assertThat(repository2.getName()).isEqualTo(repositoryResponse2.getName());
        assertThat(repository2.getId()).isEqualTo(repositoryResponse2.getId());
    }

}
