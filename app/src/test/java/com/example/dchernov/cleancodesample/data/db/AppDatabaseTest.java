package com.example.dchernov.cleancodesample.data.db;

import android.arch.persistence.room.Room;
import android.test.suitebuilder.annotation.SmallTest;

import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.data.db.models.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author dchernov
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
@SmallTest
public class AppDatabaseTest {

    private AppDatabase database;

    private UserDao userDao;
    private RepositoryDao repositoryDao;

    @Before
    public void beforeEachTest() {
        database = Room.inMemoryDatabaseBuilder(RuntimeEnvironment.application, AppDatabase.class)
                .allowMainThreadQueries()
                .build();
        userDao = database.getUserDao();
        repositoryDao = database.getRepositoryDao();
    }

    @After
    public void afterEachTest() {
        database.close();
    }

    @Test
    public void userInsert() {
        User user = new User(1, "login1");

        //insert same user to db
        long insertedId1 = userDao.insertUser(user);
        long insertedId2 = userDao.insertUser(user);

        //check that userId is not autoIncrement (user is replaced)
        assertThat(insertedId1)
                .isNotZero()
                .isEqualTo(insertedId2);
    }

    @Test
    public void userInsertAndGet() {
        //init user
        User user = new User(1, "login1");

        //insert and read user from db
        userDao.insertUser(user);
        User dbUser = userDao.getUser();

        //check that is same user
        assertThat(dbUser)
                .isNotNull()
                .isEqualTo(user);
    }

    @Test
    public void userInsertAndRemove() {
        //init user and save to db
        User user = new User(1, "login1");
        userDao.insertUser(user);

        //check deleting single user
        int deletedCount = userDao.deleteUser(user);
        assertThat(deletedCount).isEqualTo(1);

        //check that user already deleted
        deletedCount = userDao.deleteAllUsers();
        assertThat(deletedCount).isZero();
    }

    @Test
    public void repositoryInsert() {
        //init repository list
        Repository repository1 = new Repository(1, "name1");
        Repository repository2 = new Repository(2, "name2");
        List<Repository> repositoryList1 = Arrays.asList(repository1, repository2);

        //insert list to db
        List<Long> insertedIdList1 = repositoryDao.insertRepositoryList(repositoryList1);

        //check inserted count
        assertThat(insertedIdList1.size()).isEqualTo(repositoryList1.size());

        List<Repository> repositoryList2 = Arrays.asList(new Repository(2, "name3"));//same id that in repository2

        //insert new list to db and then read
        List<Long> insertedIdList2 = repositoryDao.insertRepositoryList(repositoryList2);
        List<Repository> dbRepositoryList = repositoryDao.getRepositoryListOrderByName();

        //check that repositoryList replaced
        assertThat(insertedIdList2.size()).isEqualTo(1);
        assertThat(dbRepositoryList.size()).isEqualTo(2);

    }

    @Test
    public void repositoryInsertAndGet() {
        //init repository list
        Repository repository1 = new Repository(1, "name1");
        Repository repository2 = new Repository(2, "name2");
        List<Repository> repositoryList = Arrays.asList(repository1, repository2);

        //write list to db and then read
        repositoryDao.insertRepositoryList(repositoryList);
        List<Repository> dbRepositoryList = repositoryDao.getRepositoryListOrderByName();

        //check list and items from db list
        assertThat(dbRepositoryList.size()).isEqualTo(repositoryList.size());
        assertThat(dbRepositoryList.get(0)).isEqualTo(repository1);
        assertThat(dbRepositoryList.get(1)).isEqualTo(repository2);
    }

    @Test
    public void repositoryInsertAndRemove() {
        //init repository list
        Repository repository1 = new Repository(1, "name1");
        Repository repository2 = new Repository(2, "name2");
        List<Repository> repositoryList = Arrays.asList(repository1, repository2);

        //write to db and then remove
        repositoryDao.insertRepositoryList(repositoryList);
        int deletedCount = repositoryDao.deleteRepositories(repositoryList);

        //check deleting of list
        assertThat(deletedCount).isEqualTo(repositoryList.size());

        deletedCount = repositoryDao.deleteAllRepositories();
        //check that repositories already deleted and table clean
        assertThat(deletedCount).isZero();
    }

}
