package com.example.dchernov.cleancodesample.presentation.repositories.presenter;

import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.domain.interactors.GetRepositoriesInteractor;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.RepositoriesRouter;
import com.example.dchernov.cleancodesample.presentation.repositories.view.RepositoriesView;
import com.example.dchernov.cleancodesample.presentation.repositories.view.RepositoriesView$$State;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

/**
 * @author dchernov
 */
public class RepositoriesPresenterTest {

    @Mock private RepositoriesView$$State repositoriesViewState;
    @Mock private RepositoriesView repositoriesView;
    @Mock private RepositoriesRouter repositoriesRouter;
    @Mock private GetRepositoriesInteractor getRepositoriesInteractor;

    private RepositoriesPresenterImpl repositoriesPresenter;

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);

        repositoriesPresenter = new RepositoriesPresenterImpl(repositoriesRouter, getRepositoriesInteractor);
        repositoriesPresenter.attachView(repositoriesView);
        repositoriesPresenter.setViewState(repositoriesViewState);
    }

    @Test
    public void onFirstViewAttachStartLoading_success() {

        repositoriesPresenter.onFirstViewAttach();

        InOrder inOrder = inOrder(getRepositoriesInteractor);

        inOrder.verify(getRepositoriesInteractor).setPageLoader(any());
        inOrder.verify(getRepositoriesInteractor).refresh();

        verify(repositoriesViewState).showRepositoriesProgress();
    }

    @Test
    public void moveBack_success() {
        repositoriesPresenter.clickBack();

        verify(repositoriesRouter).moveBack();
    }

    @Test
    public void moveToDetails_success() {
        Repository repository = new Repository(1, "test_repository");

        repositoriesPresenter.onItemClick(repository);

        verify(repositoriesRouter).moveToRepositoryDetails(repository);
    }

    @Test
    public void loadNextPage_success() {
        repositoriesPresenter.loadNextPage();
        verify(getRepositoriesInteractor).loadNextPage();
    }

    @Test
    public void refresh_success() {
        repositoriesPresenter.refresh();

        //calling automatically onFirstViewAttach and after by calling method refresh() in presenter
        verify(getRepositoriesInteractor, atLeast(2)).refresh();
    }

}
