package com.example.dchernov.cleancodesample.presentation.auth.signup.presenter;

import com.example.dchernov.cleancodesample.presentation.auth.signup.navigation.SignUpRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signup.view.SignUpView;
import com.example.dchernov.cleancodesample.presentation.auth.signup.view.SignUpView$$State;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * @author dchernov
 */
public class SignUpPresenterTest {

    @Mock private SignUpView$$State signUpViewState;
    @Mock private SignUpView signUpView;
    @Mock private SignUpRouter signUpRouter;

    private SignUpPresenterImpl signUpPresenter;

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);

        signUpPresenter = new SignUpPresenterImpl(signUpRouter);
        signUpPresenter.attachView(signUpView);
        signUpPresenter.setViewState(signUpViewState);
    }

    @Test
    public void signUp_register_confirm() {
        signUpPresenter.clickSignUp();

        verify(signUpRouter).confirmRegistration();
    }

    @Test
    public void signUp_moveBack() {
        signUpPresenter.moveBack();

        verify(signUpRouter).moveBack();
    }

}
