package com.example.dchernov.cleancodesample.domain.interactors;

import com.example.dchernov.cleancodesample.data.db.converters.UserConverter;
import com.example.dchernov.cleancodesample.data.net.GithubApi;
import com.example.dchernov.cleancodesample.data.net.models.requests.SignInRequestModel;
import com.example.dchernov.cleancodesample.data.net.models.responses.UserResponse;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.auth.validation.LocalAuthValidation;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulersTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.UnknownHostException;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

/**
 * @author dchernov
 */
public class SignInInteractorTest {

    @Mock private GithubApi githubApi;
    @Mock private UserRepository userRepository;
    @Mock private LocalAuthValidation localAuthValidation;

    private SignInInteractor signInInteractor;
    private UserConverter userConverter = new UserConverter();
    private RxSchedulers rxSchedulers = new RxSchedulersTest();

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);

        signInInteractor = new SignInInteractorImpl(
                githubApi, userRepository, userConverter, localAuthValidation, rxSchedulers
        );
    }

    @Test
    public void signIn_success() {

        UserResponse userResponse = new UserResponse(42, "login_text", "avatar_text");

        when(localAuthValidation.localValidate(any()))
                .thenReturn(Single.fromCallable(() -> new SignInRequestModel("")));
        when(githubApi.signIn(any())).thenReturn(Single.fromCallable(() -> userResponse));
        when(userRepository.setUser(any())).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = signInInteractor.signIn(any()).test();
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
    }

    @Test
    public void signIn_failure() {

        when(localAuthValidation.localValidate(any()))
                .thenReturn(Single.fromCallable(() -> new SignInRequestModel("")));
        when(githubApi.signIn(any())).thenReturn(Single.error(UnknownHostException::new));

        TestObserver<Void> testObserver = signInInteractor.signIn(any()).test();
        testObserver.awaitTerminalEvent();

        testObserver.assertError(UnknownHostException.class);
        testObserver.assertNotComplete();
    }

    @Test
    public void controlSignInButtonEnabling_test() {

        //mock subjects
        Subject<String> emailFieldListener = PublishSubject.create();
        Subject<String> passwordFieldListener = PublishSubject.create();

        //subscribe to button observable
        TestObserver<Boolean> buttonEnabledObserver = TestObserver.create();
        signInInteractor.controlSignInButton(emailFieldListener, passwordFieldListener)
                .subscribe(buttonEnabledObserver);

        //default: email = "", pass = ""
        buttonEnabledObserver
                .assertValueCount(1)
                .assertValues(false);

        emailFieldListener.onNext("1");

        //email = "1", pass = ""
        buttonEnabledObserver
                .assertValueCount(1)
                .assertValues(false);

        passwordFieldListener.onNext("1");

        //email = "1", pass = "1"
        buttonEnabledObserver
                .assertValueCount(2)
                .assertValues(false, true);

        passwordFieldListener.onNext("123");

        //email = "1", pass = "123"
        buttonEnabledObserver
                .assertValueCount(2)
                .assertValues(false, true);

        passwordFieldListener.onNext("");

        //email = "1", pass = ""
        buttonEnabledObserver
                .assertValueCount(3)
                .assertValues(false, true, false);

        emailFieldListener.onNext("");

        buttonEnabledObserver
                .assertValueCount(3)
                .assertValues(false, true, false);

        emailFieldListener.onNext("");
        passwordFieldListener.onNext("1");

        //email = "", pass = "1"
        buttonEnabledObserver
                .assertValueCount(3)
                .assertValues(false, true, false);

        emailFieldListener.onNext("1");
        passwordFieldListener.onNext("1");

        //email = "1", pass = "1"
        buttonEnabledObserver
                .assertValueCount(4)
                .assertValues(false, true, false, true);

    }

}
