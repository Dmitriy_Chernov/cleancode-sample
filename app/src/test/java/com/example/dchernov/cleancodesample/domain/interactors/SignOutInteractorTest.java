package com.example.dchernov.cleancodesample.domain.interactors;

import com.example.dchernov.cleancodesample.data.repositories.ReposRepository;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulersTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author dchernov
 */
public class SignOutInteractorTest {

    private RxSchedulers rxSchedulers = new RxSchedulersTest();
    @Mock private UserRepository userRepository;
    @Mock private ReposRepository reposRepository;

    private SignOutInteractor signOutInteractor;

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);
        signOutInteractor = new SignOutInteractorImpl(rxSchedulers, userRepository, reposRepository);
    }

    @Test
    public void signOut_success() {

        when(userRepository.deleteUsers()).thenReturn(Completable.complete());
        when(reposRepository.deleteRepositories()).thenReturn(Completable.complete());

        TestObserver<Completable> testObserver = TestObserver.create();

        signOutInteractor.signOut().subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
    }

    @Test
    public void signOut_deleteUsersError() {

        //throwable at users deleting
        when(userRepository.deleteUsers()).thenReturn(Completable.error(UnknownError::new));
        when(reposRepository.deleteRepositories()).thenReturn(Completable.complete());

        TestObserver<Completable> testObserver = TestObserver.create();

        signOutInteractor.signOut().subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertError(UnknownError.class);
        testObserver.assertNotComplete();
    }

    @Test
    public void signOut_deleteRepositoriesError() {

        //throwable at repositories deleting
        when(userRepository.deleteUsers()).thenReturn(Completable.complete());
        when(reposRepository.deleteRepositories()).thenReturn(Completable.error(UnknownError::new));

        TestObserver<Completable> testObserver = TestObserver.create();

        signOutInteractor.signOut().subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertError(UnknownError.class);
        testObserver.assertNotComplete();
    }

    @Test
    public void signOut_deleteRepositoriesAndDeleteUsersError() {

        //throwable at repositories deleting and users deleting
        when(userRepository.deleteUsers()).thenReturn(Completable.error(NullPointerException::new));
        when(reposRepository.deleteRepositories()).thenReturn(Completable.error(UnknownError::new));

        TestObserver<Completable> testObserver = TestObserver.create();

        signOutInteractor.signOut().subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        //receive single throwable
        assertThat(testObserver.errorCount()).isEqualTo(1);
        testObserver.assertError(NullPointerException.class);
        testObserver.assertNotComplete();
    }

}
