package com.example.dchernov.cleancodesample.presentation.splash.presenter;

import com.example.dchernov.cleancodesample.data.db.models.User;
import com.example.dchernov.cleancodesample.domain.interactors.GetUserInteractor;
import com.example.dchernov.cleancodesample.presentation.splash.navigation.SplashRouter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author dchernov
 */
public class SplashPresenterTest {

    @Mock private SplashRouter splashRouter;
    @Mock private GetUserInteractor getUserInteractor;

    private SplashPresenterImpl splashPresenter;

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);
        splashPresenter = new SplashPresenterImpl();
    }

    @Test
    public void getUser_success() {

        when(getUserInteractor.getUser()).thenReturn(Single.fromCallable(() -> new User(1, "test_user")));


        splashPresenter.setData(splashRouter, getUserInteractor);
        splashPresenter.checkAuth();

        verify(splashRouter).moveToRepositories();
        verify(splashRouter, never()).moveToAuth();
        verify(splashRouter, never()).moveBack();
    }

    @Test
    public void getUser_failure() {

        when(getUserInteractor.getUser()).thenReturn(Single.error(NullPointerException::new));

        splashPresenter.setData(splashRouter, getUserInteractor);
        splashPresenter.checkAuth();

        verify(splashRouter).moveToAuth();
        verify(splashRouter, never()).moveToRepositories();
        verify(splashRouter, never()).moveBack();
    }

    @Test
    public void splashClickBack() {

        //click back twice before initialization of app component
        splashPresenter.clickBack();
        splashPresenter.clickBack();

        splashPresenter.setData(splashRouter, getUserInteractor);

        verify(splashRouter, atLeast(2)).moveBack();
        verify(splashRouter, never()).moveToAuth();
        verify(splashRouter, never()).moveToRepositories();
    }

}
