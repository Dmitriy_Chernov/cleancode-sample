package com.example.dchernov.cleancodesample.domain.auth.validation;

import android.content.Context;

import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.data.net.models.requests.SignInRequestModel;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.SignInFullDataModel;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.ValidateErrorModel;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.observers.TestObserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author dchernov
 */
public class LocalAuthValidationTest {

    private LocalAuthValidation localAuthValidation;
    private Context context;

    @Before
    public void beforeEachTest() {

        context = mock(Context.class);

        when(context.getString(R.string.validation_password_field_empty)).thenReturn("empty pass");
        when(context.getString(R.string.validation_password_field_short_length)).thenReturn("short pass");
        when(context.getString(R.string.validation_email_field_empty)).thenReturn("empty email");
        when(context.getString(R.string.validation_email_field_invalid_format)).thenReturn("invalid email");

        localAuthValidation = new LocalAuthValidation(context);
    }

    @Test
    public void emailLocalValidate_empty() {
        ValidateErrorModel validateErrorModel = localAuthValidation.emailLocalValidate("");

        assertThat(validateErrorModel).isNotNull();
        assertThat(validateErrorModel.getField()).isEqualTo(ValidateErrorModel.Field.EMAIL);
        assertThat(validateErrorModel.getDescription()).isEqualTo(context.getString(R.string.validation_email_field_empty));
    }

    @Test
    public void emailLocalValidate_notValid() {
        ValidateErrorModel validateErrorModel = localAuthValidation.emailLocalValidate("no_reply@gmail");

        assertThat(validateErrorModel).isNotNull();
        assertThat(validateErrorModel.getField()).isEqualTo(ValidateErrorModel.Field.EMAIL);
        assertThat(validateErrorModel.getDescription()).isEqualTo(context.getString(R.string.validation_email_field_invalid_format));
    }

    @Test
    public void emailLocalValidate_valid() {
        ValidateErrorModel validateErrorModel = localAuthValidation.emailLocalValidate("no_reply@gmail.com");

        assertThat(validateErrorModel).isNull();
    }

    @Test
    public void passwordLocalValidate_empty() {
        ValidateErrorModel validateErrorModel = localAuthValidation.passwordLocalValidate("");

        assertThat(validateErrorModel).isNotNull();
        assertThat(validateErrorModel.getField()).isEqualTo(ValidateErrorModel.Field.PASSWORD);
        assertThat(validateErrorModel.getDescription()).isEqualTo(context.getString(R.string.validation_password_field_empty));
    }

    @Test
    public void passwordLocalValidate_shortLength() {
        ValidateErrorModel validateErrorModel = localAuthValidation.passwordLocalValidate("123");

        assertThat(validateErrorModel).isNotNull();
        assertThat(validateErrorModel.getField()).isEqualTo(ValidateErrorModel.Field.PASSWORD);
        assertThat(validateErrorModel.getDescription()).isEqualTo(context.getString(R.string.validation_password_field_short_length));
    }

    @Test
    public void localValidate_errorValidation() {

        SignInFullDataModel invalidModel = new SignInFullDataModel("no_reply@gmail", "123");

        //subscribe to testObserver
        TestObserver<SignInRequestModel> testObserver = TestObserver.create();
        localAuthValidation.localValidate(invalidModel).subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertError(AuthValidateException.class);
        AuthValidateException authValidateException =
                (AuthValidateException) testObserver.errors().get(0);

        assertThat(authValidateException.getValidateErrorList())
                .isNotNull()
                .hasSize(2)
                .extracting("field", "description")
                .containsOnly(
                        (tuple(ValidateErrorModel.Field.EMAIL, context.getString(R.string.validation_email_field_invalid_format))),
                        (tuple(ValidateErrorModel.Field.PASSWORD, context.getString(R.string.validation_password_field_short_length)))
                );

    }

    @Test
    public void localValidate_successValidation() {

        SignInFullDataModel validModel = new SignInFullDataModel("no_reply@gmail.com", "1234");

        TestObserver<SignInRequestModel> testObserver = TestObserver.create();
        localAuthValidation.localValidate(validModel).subscribe(testObserver);
        testObserver.awaitTerminalEvent();

        testObserver.assertComplete();
        testObserver.assertNoErrors();

        //check data from after validation
        SignInRequestModel signInRequestModel = testObserver.values().get(0);

        assertThat(signInRequestModel.getToken()).isNotEmpty();
    }

}
