package com.example.dchernov.cleancodesample.presentation.auth.signin.presenter;

import com.example.dchernov.cleancodesample.domain.interactors.SignInInteractor;
import com.example.dchernov.cleancodesample.presentation.auth.signin.navigation.SignInRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signin.view.SignInView;
import com.example.dchernov.cleancodesample.presentation.auth.signin.view.SignInView$$State;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.UnknownHostException;

import io.reactivex.Completable;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author dchernov
 */
public class SignInPresenterTest {

    @Mock private SignInRouter signInRouter;
    @Mock private SignInInteractor signInInteractor;
    @Mock private SignInView signInView;
    @Mock private SignInView$$State signInViewState;

    private SignInPresenterImpl signInPresenter;

    @Before
    public void beforeEachTest() {
        MockitoAnnotations.initMocks(this);

        signInPresenter = new SignInPresenterImpl(signInInteractor, signInRouter);
        signInPresenter.attachView(signInView);
        signInPresenter.setViewState(signInViewState);
    }

    @Test
    public void signIn_success() {
        when(signInInteractor.signIn(any())).thenReturn(Completable.complete());

        signInPresenter.clickSignIn(any());

        InOrder inOrder = inOrder(signInViewState);
        inOrder.verify(signInViewState).showSignInProgress(true);
        inOrder.verify(signInViewState).showSignInProgress(false);
        inOrder.verify(signInViewState).showSignInSuccess();

        verify(signInRouter).showRepositories();
    }

    @Test
    public void signIn_failure() {
        when(signInInteractor.signIn(any())).thenReturn(Completable.error(UnknownHostException::new));

        signInPresenter.clickSignIn(any());

        InOrder inOrder = inOrder(signInViewState);
        inOrder.verify(signInViewState).showSignInProgress(true);
        inOrder.verify(signInViewState).showSignInProgress(false);
        inOrder.verify(signInViewState).showUnknownError(any());

        verifyNoMoreInteractions(signInRouter);
    }

    @Test
    public void signIn_moveBack() {
        signInPresenter.clickBack();

        verify(signInRouter).moveBack();
    }

    @Test
    public void signIn_moveToSignUp() {
        signInPresenter.clickSignUp();

        verify(signInRouter).moveToRegistration();
    }

}
