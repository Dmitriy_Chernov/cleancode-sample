package com.example.dchernov.cleancodesample.presentation.auth.signin.presenter;

import com.example.dchernov.cleancodesample.presentation.auth.signin.models.SignInFullDataModel;

import io.reactivex.Observable;

/**
 * @author dchernov
 */
public interface SignInPresenter {

    void clickSignIn(SignInFullDataModel signInFullDataModel);
    void clickSignUp();

    void listenInputFields(Observable<String> emailFieldListener,
                           Observable<String> passwordFieldListener);

    void clickBack();

}
