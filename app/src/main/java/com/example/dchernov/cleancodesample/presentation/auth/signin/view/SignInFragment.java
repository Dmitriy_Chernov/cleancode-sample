package com.example.dchernov.cleancodesample.presentation.auth.signin.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.di.ComponentStorage;
import com.example.dchernov.cleancodesample.domain.interactors.SignInInteractor;
import com.example.dchernov.cleancodesample.presentation.BackPressedListener;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.SignInFullDataModel;
import com.example.dchernov.cleancodesample.presentation.auth.signin.navigation.SignInRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signin.presenter.SignInPresenterImpl;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

/**
 * @author dchernov
 */
public class SignInFragment extends MvpAppCompatFragment implements SignInView, BackPressedListener {

    @Inject
    SignInInteractor signInInteractor;

    @Inject
    SignInRouter signInRouter;

    @InjectPresenter
    SignInPresenterImpl signInPresenter;

    @ProvidePresenter
    SignInPresenterImpl provideSignInPresenter() {
        return new SignInPresenterImpl(signInInteractor, signInRouter);
    }

    private EditText loginInput;
    private EditText passwordInput;
    private Button signInBtn;
    private Button signUpBtn;
    private ProgressBar progressBar;

    // The Idling Resource which will be null in production.
    @Nullable
    private CountingIdlingResource idlingResource;

    public static SignInFragment newInstance() {
        Bundle args = new Bundle();
        SignInFragment fragment = new SignInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ComponentStorage.getInstance().getAuthComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fmt_sign_in, container, false);

        progressBar = view.findViewById(R.id.progress);
        loginInput = view.findViewById(R.id.et_login);
        passwordInput = view.findViewById(R.id.et_password);
        signInBtn = view.findViewById(R.id.btn_sign_in);
        signUpBtn = view.findViewById(R.id.btn_sign_up);

        signUpBtn.setOnClickListener(v -> signInPresenter.clickSignUp());
        signInBtn.setOnClickListener(v -> {
            SignInFullDataModel model = new SignInFullDataModel(
                    loginInput.getText().toString(),
                    passwordInput.getText().toString());

            signInPresenter.clickSignIn(model);
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        signInPresenter.listenInputFields(
                RxTextView.textChanges(loginInput).map(CharSequence::toString),
                RxTextView.textChanges(passwordInput).map(CharSequence::toString)
        );
    }

    @Override
    public void showSignInProgress(boolean isShow) {

        int visibleInProgress = isShow? View.VISIBLE : View.GONE;
        int hideInProgress = isShow? View.GONE: View.VISIBLE;

        progressBar.setVisibility(visibleInProgress);

        signInBtn.setVisibility(hideInProgress);
        signUpBtn.setVisibility(hideInProgress);
        loginInput.setVisibility(hideInProgress);
        passwordInput.setVisibility(hideInProgress);

        updateIdlingResourceStateIfNeeded(isShow);
    }

    @Override
    public void showEmailError(String errorText) {
        loginInput.setError(errorText);
    }

    @Override
    public void showPasswordError(String errorText) {
        passwordInput.setError(errorText);
    }

    @Override
    public void showUnknownError(String errorText) {
        Toast.makeText(getContext(), String.format(getString(R.string.unkown_error), errorText), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showSignInSuccess() {
        Toast.makeText(getContext(), R.string.fmt_sign_in_success_msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setEnableSignInButton(boolean isEnabled) {
        signInBtn.setEnabled(isEnabled);
    }

    @Override
    public boolean onBackPressed() {
        signInPresenter.clickBack();
        return true;
    }

    /*
    method for blocking Espresso tests when view in progress
     */
    private void updateIdlingResourceStateIfNeeded(boolean isIdle) {

        if (idlingResource == null) {
            return;
        }

        if (isIdle) {
            idlingResource.increment();
        } else  {
            idlingResource.decrement();
        }
    }

    @VisibleForTesting
    @NonNull
    public CountingIdlingResource getIdlingResource() {
        if (idlingResource == null) {
            idlingResource = new CountingIdlingResource(getClass().getName());
        }
        return idlingResource;
    }

}
