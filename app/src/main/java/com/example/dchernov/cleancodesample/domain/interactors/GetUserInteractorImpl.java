package com.example.dchernov.cleancodesample.domain.interactors;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.data.db.models.User;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;

import io.reactivex.Single;

/**
 * @author dchernov
 */
public class GetUserInteractorImpl implements GetUserInteractor {

    private UserRepository userRepository;
    private RxSchedulers rxSchedulers;

    public GetUserInteractorImpl(@NonNull UserRepository userRepository,
                                 @NonNull RxSchedulers rxSchedulers) {
        this.userRepository = userRepository;
        this.rxSchedulers = rxSchedulers;
    }

    @Override
    public Single<User> getUser() {
        return userRepository.getUser()
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler());
    }

}
