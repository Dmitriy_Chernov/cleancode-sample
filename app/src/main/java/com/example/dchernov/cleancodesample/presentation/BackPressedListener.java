package com.example.dchernov.cleancodesample.presentation;

/**
 * @author dchernov
 */
public interface BackPressedListener {

    /**
     *
     * @return true if the callback consumed the back press, false otherwise.
     */
    boolean onBackPressed();

}
