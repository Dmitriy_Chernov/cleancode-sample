package com.example.dchernov.cleancodesample.presentation.auth.signin.models;

import android.support.annotation.NonNull;

/**
 * @author dchernov
 */
public class SignInFullDataModel {

    private String email;
    private String password;

    public SignInFullDataModel(@NonNull String email, @NonNull String password) {
        this.email = email;
        this.password = password;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    @NonNull
    public String getPassword() {
        return password;
    }
}
