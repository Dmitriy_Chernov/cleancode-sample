package com.example.dchernov.cleancodesample.domain.interactors;

/**
 * @author dchernov
 */
public interface PageLoader<T> {

    void onGetNextPage(T t);

    void onThrowable(Throwable throwable);

    void onClear();

    void showPageProgress();

    void hidePageProgress();

}
