package com.example.dchernov.cleancodesample.presentation.repositories.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.presentation.repositories.presenter.RepositoriesPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dchernov
 */
public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepositoriesViewHolder> {

    private List<Repository> repositoryList = new ArrayList<>();
    private final LayoutInflater layoutInflater;

    @Nullable
    private RepositoriesPresenter repositoriesPresenter;

    public RepositoriesAdapter(Context context) {
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addRepositoryList(@NonNull List<Repository> repositoryList) {
        int oldSize = this.repositoryList.size();
        int newItemsCount = repositoryList.size();

        this.repositoryList.addAll(repositoryList);

        notifyItemChanged(oldSize-1);
        notifyItemRangeInserted(oldSize, newItemsCount);
    }

    public void clearRepositoryList() {
        this.repositoryList.clear();
        notifyDataSetChanged();
    }

    public void setRepositoriesPresenter(@NonNull RepositoriesPresenter repositoriesPresenter) {
        this.repositoriesPresenter = repositoriesPresenter;
    }

    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_repository, parent, false);
        return new RepositoriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoriesViewHolder holder, int position) {
        Repository repository = repositoryList.get(position);
        boolean showProgress = showPageProgress(position);

        holder.bind(repository, repositoriesPresenter, showProgress);

        if (repositoriesPresenter!=null && needMoreItems(position)) {
            repositoriesPresenter.loadNextPage();
        }
    }

    @Override
    public int getItemCount() {
        return (repositoryList != null) ? repositoryList.size() : 0;
    }

    private boolean showPageProgress(int position) {
        if (repositoriesPresenter==null) {
            return false;
        }

        boolean isLastPosition = position == getItemCount()-1;
        return isLastPosition && repositoriesPresenter.showPageProgress();
    }

    private boolean needMoreItems(int position) {
        return (position>=getItemCount()/2);
    }

    static class RepositoriesViewHolder extends RecyclerView.ViewHolder {

        TextView nameTv;
        ProgressBar progressBar;

        RepositoriesViewHolder(View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.tv_name);
            progressBar = itemView.findViewById(R.id.progressBar);
        }

        private void bind(Repository repository, RepositoriesPresenter repositoriesPresenter,
                          boolean showPageProgress) {
            nameTv.setText(repository.getName());

            itemView.setOnClickListener(view -> {
                if (repositoriesPresenter!=null) {
                    repositoriesPresenter.onItemClick(repository);
                }
            });

            progressBar.setVisibility(showPageProgress? View.VISIBLE: View.GONE);
        }
    }

}
