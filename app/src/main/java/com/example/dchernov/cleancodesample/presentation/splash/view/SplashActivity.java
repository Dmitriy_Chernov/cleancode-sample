package com.example.dchernov.cleancodesample.presentation.splash.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.di.ComponentStorage;
import com.example.dchernov.cleancodesample.domain.interactors.GetUserInteractor;
import com.example.dchernov.cleancodesample.presentation.auth.view.AuthActivity;
import com.example.dchernov.cleancodesample.presentation.repositories.view.RepositoriesActivity;
import com.example.dchernov.cleancodesample.presentation.splash.navigation.SplashRouter;
import com.example.dchernov.cleancodesample.presentation.splash.presenter.SplashPresenterImpl;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;

import static com.example.dchernov.cleancodesample.di.NavigationRoutes.AUTH_SCREEN;
import static com.example.dchernov.cleancodesample.di.NavigationRoutes.REPOSITORIES_SCREEN;

public class SplashActivity extends MvpAppCompatActivity implements SplashView {

    @Inject
    SplashRouter splashRouter;

    @Inject
    NavigatorHolder navigatorHolder;

    @Inject
    GetUserInteractor getUserInteractor;

    @InjectPresenter
    SplashPresenterImpl splashPresenter;

    @Nullable
    private Disposable injectDisposable;

    private Navigator navigator = new SupportAppNavigator(this, R.id.splash_container) {

        @Override
        protected Bundle createStartActivityOptions(Command command, Intent activityIntent) {
            return ActivityOptionsCompat.makeCustomAnimation(getApplication(),
                    android.R.anim.fade_in, android.R.anim.fade_out).toBundle();
        }

        @Override
        protected Intent createActivityIntent(String screenKey, Object data) {
            Class<? extends Activity> activityClass = null;
            switch (screenKey) {
                case AUTH_SCREEN:
                    activityClass = AuthActivity.class;
                    break;
                case REPOSITORIES_SCREEN:
                    activityClass = RepositoriesActivity.class;
                    break;
            }

            return new Intent(SplashActivity.this, activityClass);
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            return null;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        startLazyInjection();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startNavigator();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopNavigator();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        stopLazyInjection();

        if (isFinishing()) {
            ComponentStorage.getInstance().clearSplashComponent();
        }
    }

    @Override
    public void onBackPressed() {
        splashPresenter.clickBack();
    }

    private void startLazyInjection() {

        stopLazyInjection();

        injectDisposable = ComponentStorage.getInstance().initAppComponentLazy(getApplication())
                .doOnComplete(() -> {
                    //app component created
                    ComponentStorage.getInstance().getSplashComponent().inject(this);

                    startNavigator();

                    splashPresenter.setData(splashRouter, getUserInteractor);
                    splashPresenter.checkAuth();

                })
                .doOnDispose(this::stopNavigator)
                .subscribe(() -> {}, throwable -> {});
    }

    private void stopLazyInjection() {
        if (injectDisposable != null) {
            injectDisposable.dispose();
        }
    }

    private void startNavigator() {
        if (navigatorHolder!=null) {
            navigatorHolder.setNavigator(navigator);
        }
    }

    private void stopNavigator() {
        if (navigatorHolder!=null) {
            navigatorHolder.removeNavigator();
        }
    }

}
