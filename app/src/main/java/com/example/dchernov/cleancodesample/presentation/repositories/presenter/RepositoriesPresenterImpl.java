package com.example.dchernov.cleancodesample.presentation.repositories.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.domain.interactors.GetRepositoriesInteractor;
import com.example.dchernov.cleancodesample.domain.interactors.PageLoader;
import com.example.dchernov.cleancodesample.presentation.BasePresenter;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.RepositoriesRouter;
import com.example.dchernov.cleancodesample.presentation.repositories.view.RepositoriesView;

import java.util.List;

/**
 * @author dchernov
 */
@InjectViewState
public class RepositoriesPresenterImpl extends BasePresenter<RepositoriesView> implements
        RepositoriesPresenter {

    private RepositoriesRouter repositoriesRouter;
    private GetRepositoriesInteractor getRepositoriesInteractor;

    private boolean showPageProgress;

    public RepositoriesPresenterImpl(@NonNull RepositoriesRouter repositoriesRouter,
                                     @NonNull GetRepositoriesInteractor getRepositoriesInteractor) {
        this.repositoriesRouter = repositoriesRouter;
        this.getRepositoriesInteractor = getRepositoriesInteractor;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().showRepositoriesProgress();
        getRepositoriesInteractor.setPageLoader(new PageLoader<List<Repository>>() {

            @Override
            public void onGetNextPage(List<Repository> repositoryList) {
                getViewState().hideRepositoriesProgress();
                getViewState().addRepositories(repositoryList);
            }

            @Override
            public void onThrowable(Throwable throwable) {
                getViewState().hideRepositoriesProgress();
                getViewState().showRepositoriesUnknownError(throwable.getLocalizedMessage());
            }

            @Override
            public void onClear() {
                getViewState().clearRepositories();
            }

            @Override
            public void showPageProgress() {
                showPageProgress = true;
            }

            @Override
            public void hidePageProgress() {
                showPageProgress = false;
            }

        });
        getRepositoriesInteractor.refresh();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getRepositoriesInteractor.clear();
    }

    @Override
    public void onItemClick(Repository repository) {
        repositoriesRouter.moveToRepositoryDetails(repository);
    }

    @Override
    public void clickBack() {
        repositoriesRouter.moveBack();
    }

    @Override
    public void refresh() {
        getRepositoriesInteractor.refresh();
    }

    @Override
    public void loadNextPage() {
        getRepositoriesInteractor.loadNextPage();
    }

    @Override
    public boolean showPageProgress() {
        return showPageProgress;
    }

}
