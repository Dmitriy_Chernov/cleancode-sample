package com.example.dchernov.cleancodesample.di.app;

import android.content.Context;

import com.example.dchernov.cleancodesample.data.db.AppDatabase;
import com.example.dchernov.cleancodesample.data.db.RepositoryDao;
import com.example.dchernov.cleancodesample.data.db.UserDao;
import com.example.dchernov.cleancodesample.data.repositories.ReposRepository;
import com.example.dchernov.cleancodesample.data.repositories.ReposRepositoryImpl;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.data.repositories.UserRepositoryImpl;

import dagger.Module;
import dagger.Provides;

/**
 * @author dchernov
 */
@Module
public class DbModule {

    @Provides
    @PerApplication
    AppDatabase provideAppDatabase(Context context) {
        return AppDatabase.build(context);
    }

    @Provides
    @PerApplication
    UserRepository provideUserRepository(AppDatabase appDatabase) {
        UserDao userDao = appDatabase.getUserDao();
        return new UserRepositoryImpl(userDao);
    }

    @Provides
    @PerApplication
    ReposRepository provideReposRepository(AppDatabase appDatabase) {
        RepositoryDao repositoryDao = appDatabase.getRepositoryDao();
        return new ReposRepositoryImpl(repositoryDao);
    }

}
