package com.example.dchernov.cleancodesample.domain.interactors;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.data.db.models.Repository;

import java.util.List;

/**
 * @author dchernov
 */
public interface GetRepositoriesInteractor {

    void setPageLoader(@NonNull PageLoader<List<Repository>> pageLoader);

    void loadNextPage();

    void refresh();

    void clear();

}
