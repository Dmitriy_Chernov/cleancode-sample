package com.example.dchernov.cleancodesample.data.repositories;

import com.example.dchernov.cleancodesample.data.db.RepositoryDao;
import com.example.dchernov.cleancodesample.data.db.models.Repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author dchernov
 */
public class ReposRepositoryImpl implements ReposRepository {

    private RepositoryDao repositoryDao;

    public ReposRepositoryImpl(RepositoryDao repositoryDao) {
        this.repositoryDao = repositoryDao;
    }

    @Override
    public Single<List<Repository>> getRepositoryList() {
        return Single.fromCallable(() -> repositoryDao.getRepositoryListOrderByName());
    }

    @Override
    public Completable updateRepository(Repository repository) {
        return Completable.fromCallable(() -> repositoryDao.updateRepository(repository));
    }

    @Override
    public Single<List<Long>> insertRepositoryList(List<Repository> repositoryList) {
        return Single.fromCallable(() -> repositoryDao.insertRepositoryList(repositoryList));
    }

    @Override
    public Completable deleteRepositories() {
        return Completable.fromCallable(() -> repositoryDao.deleteAllRepositories());
    }

    @Override
    public Completable deleteRepositories(List<Repository> repositoryList) {
        return Completable.fromCallable(() -> repositoryDao.deleteRepositories(repositoryList));
    }

}
