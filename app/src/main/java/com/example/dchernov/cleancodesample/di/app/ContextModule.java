package com.example.dchernov.cleancodesample.di.app;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * @author dchernov
 */
@Module
public class ContextModule {

    private Application application;

    public ContextModule(Application application) {
        this.application = application;
    }

    @Provides
    @PerApplication
    Context provideAppContext() {
        return application;
    }

}
