package com.example.dchernov.cleancodesample.presentation.auth.signup.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.di.ComponentStorage;
import com.example.dchernov.cleancodesample.presentation.BackPressedListener;
import com.example.dchernov.cleancodesample.presentation.auth.signup.navigation.SignUpRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signup.presenter.SignUpPresenterImpl;

import javax.inject.Inject;

/**
 * @author dchernov
 */
public class SignUpFragment extends MvpAppCompatFragment implements SignUpView, BackPressedListener {

    @Inject
    SignUpRouter signUpRouter;

    @InjectPresenter
    SignUpPresenterImpl signUpPresenter;

    @ProvidePresenter
    SignUpPresenterImpl provideSignUpPresenter() {
        return new SignUpPresenterImpl(signUpRouter);
    }

    public static SignUpFragment newInstance() {
        Bundle args = new Bundle();
        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ComponentStorage.getInstance().getAuthComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fmt_sign_up, container, false);

        Button signUpButton = view.findViewById(R.id.btn_sign_up);
        signUpButton.setOnClickListener(v -> signUpPresenter.clickSignUp());

        return view;
    }

    @Override
    public boolean onBackPressed() {
        signUpPresenter.moveBack();
        return true;
    }
}
