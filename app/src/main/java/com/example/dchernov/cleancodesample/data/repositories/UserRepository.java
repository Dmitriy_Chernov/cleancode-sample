package com.example.dchernov.cleancodesample.data.repositories;

import com.example.dchernov.cleancodesample.data.db.models.User;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author dchernov
 */
public interface UserRepository {

    Single<User> getUser();

    Completable setUser(User user);

    Completable deleteUsers();

    Completable deleteUser(User user);

}
