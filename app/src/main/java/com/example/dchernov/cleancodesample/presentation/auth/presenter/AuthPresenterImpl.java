package com.example.dchernov.cleancodesample.presentation.auth.presenter;

import com.arellomobile.mvp.MvpPresenter;
import com.example.dchernov.cleancodesample.presentation.BasePresenter;
import com.example.dchernov.cleancodesample.presentation.auth.navigation.AuthRouter;
import com.example.dchernov.cleancodesample.presentation.auth.view.AuthView;

/**
 * @author dchernov
 */
public class AuthPresenterImpl extends BasePresenter<AuthView> implements AuthPresenter {

    private AuthRouter authRouter;

    public AuthPresenterImpl(AuthRouter authRouter) {
        this.authRouter = authRouter;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        authRouter.moveToSignIn();
    }

}
