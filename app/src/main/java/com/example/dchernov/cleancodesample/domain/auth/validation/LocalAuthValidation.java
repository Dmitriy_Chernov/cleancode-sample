package com.example.dchernov.cleancodesample.domain.auth.validation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.data.net.models.requests.SignInRequestModel;
import com.example.dchernov.cleancodesample.domain.Base64Coder;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.SignInFullDataModel;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.ValidateErrorModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Single;

/**
 * @author dchernov
 */
public class LocalAuthValidation {

    private static final int PASSWORD_MIN_LENGTH = 4;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @NonNull
    private Context context;

    public LocalAuthValidation(@NonNull Context context) {
        this.context = context;
    }

    public Single<SignInRequestModel> localValidate(@NonNull SignInFullDataModel signInFullDataModel) {

        final Callable<SignInFullDataModel> signInFilledModelCallable = () -> signInFullDataModel;

        return Single.fromCallable(signInFilledModelCallable)
                .map(this::getLocalValidateErrors)
                .flatMap(validateErrorModels -> {
                    if (!validateErrorModels.isEmpty()) {
                        return Single.error(new AuthValidateException(validateErrorModels));
                    } else {
                        return Single.fromCallable(signInFilledModelCallable);
                    }
                })
                .map(this::mapModel);
    }

    @NonNull
    private List<ValidateErrorModel> getLocalValidateErrors(@NonNull SignInFullDataModel signInFullDataModel) {
        List<ValidateErrorModel> validateErrorModelList = new ArrayList<>();

        ValidateErrorModel localValidateErrorModel;

        localValidateErrorModel = emailLocalValidate(signInFullDataModel.getEmail());
        addNonNullModelToList(localValidateErrorModel, validateErrorModelList);

        localValidateErrorModel = passwordLocalValidate(signInFullDataModel.getPassword());
        addNonNullModelToList(localValidateErrorModel, validateErrorModelList);

        return validateErrorModelList;
    }

    @Nullable
    ValidateErrorModel emailLocalValidate(@NonNull String email) {
        if (email.length() == 0) {
            return new ValidateErrorModel(ValidateErrorModel.Field.EMAIL, context.getString(R.string.validation_email_field_empty));
        }
        if (!email.matches(EMAIL_PATTERN)) {
            return new ValidateErrorModel(ValidateErrorModel.Field.EMAIL, context.getString(R.string.validation_email_field_invalid_format));
        }
        return null;
    }

    @Nullable
    ValidateErrorModel passwordLocalValidate(@NonNull String password) {
        if (password.length() == 0) {
            return new ValidateErrorModel(ValidateErrorModel.Field.PASSWORD, context.getString(R.string.validation_password_field_empty));
        }
        if (password.length() < PASSWORD_MIN_LENGTH) {
            return new ValidateErrorModel(ValidateErrorModel.Field.PASSWORD, context.getString(R.string.validation_password_field_short_length));
        }
        return null;
    }

    private void addNonNullModelToList(@Nullable ValidateErrorModel localValidateErrorModel,
                                       @NonNull List<ValidateErrorModel> validateErrorModelList) {
        if (localValidateErrorModel != null) {
            validateErrorModelList.add(localValidateErrorModel);
        }
    }

    @NonNull
    private SignInRequestModel mapModel(@NonNull SignInFullDataModel signInFullDataModel) {

        String email = signInFullDataModel.getEmail();
        String password = signInFullDataModel.getPassword();

        String credentials = String.format("%s:%s", email, password);
        String token = "Basic "+ Arrays.toString(Base64Coder.encode(credentials.getBytes()));

        return new SignInRequestModel(token);
    }

}
