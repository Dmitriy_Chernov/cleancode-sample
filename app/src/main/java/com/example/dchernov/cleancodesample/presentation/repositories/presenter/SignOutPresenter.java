package com.example.dchernov.cleancodesample.presentation.repositories.presenter;

/**
 * @author dchernov
 */
public interface SignOutPresenter {

    void clickSignOut();

}
