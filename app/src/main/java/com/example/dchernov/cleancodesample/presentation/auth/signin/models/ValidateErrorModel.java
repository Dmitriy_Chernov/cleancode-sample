package com.example.dchernov.cleancodesample.presentation.auth.signin.models;

import android.support.annotation.NonNull;

/**
 * @author dchernov
 */
public class ValidateErrorModel {

    public enum Field {
        EMAIL,
        PASSWORD
    }

    private Field field;
    private String description;

    public ValidateErrorModel(@NonNull Field field, @NonNull String description) {
        this.field = field;
        this.description = description;
    }

    @NonNull
    public Field getField() {
        return field;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

}
