package com.example.dchernov.cleancodesample.presentation.repositories.presenter;

import com.example.dchernov.cleancodesample.data.db.models.Repository;

/**
 * @author dchernov
 */
public interface RepositoriesPresenter {

    void onItemClick(Repository repository);

    void clickBack();

    void refresh();

    void loadNextPage();

    boolean showPageProgress();

}
