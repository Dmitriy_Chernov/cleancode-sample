package com.example.dchernov.cleancodesample;

import android.app.Application;

/**
 * @author dchernov
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
