package com.example.dchernov.cleancodesample.presentation.auth.signup.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.dchernov.cleancodesample.presentation.auth.signup.navigation.SignUpRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signup.view.SignUpView;

/**
 * @author dchernov
 */
@InjectViewState
public class SignUpPresenterImpl extends MvpPresenter<SignUpView> implements SignUpPresenter {

    private SignUpRouter signUpRouter;

    public SignUpPresenterImpl(@NonNull SignUpRouter signUpRouter) {
        this.signUpRouter = signUpRouter;
    }

    @Override
    public void clickSignUp() {
        // TODO: must be implemented registration process
        signUpRouter.confirmRegistration();
    }

    @Override
    public void moveBack() {
        signUpRouter.moveBack();
    }
}
