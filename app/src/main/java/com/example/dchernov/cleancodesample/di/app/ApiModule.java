package com.example.dchernov.cleancodesample.di.app;

import com.example.dchernov.cleancodesample.data.net.GithubApi;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author dchernov
 */
@Module
public class ApiModule {

    @Provides
    @PerApplication
    public GithubApi provideApi(Retrofit retrofit) {
        return retrofit.create(GithubApi.class);
    }

    @Provides
    @PerApplication
    Retrofit provideRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(GithubApi.API_URL)
                .build();
    }

    @Provides
    @PerApplication
    OkHttpClient provideOkHttpClient() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @PerApplication
    Gson provideGson() {
        return new Gson();
    }

}
