package com.example.dchernov.cleancodesample.di.auth;

import com.example.dchernov.cleancodesample.di.reusable.CiceroneModule;
import com.example.dchernov.cleancodesample.presentation.auth.navigation.AuthRouter;
import com.example.dchernov.cleancodesample.presentation.auth.navigation.AuthRouterImpl;
import com.example.dchernov.cleancodesample.presentation.auth.signin.navigation.SignInRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signup.navigation.SignUpRouter;

import dagger.Lazy;
import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Router;

/**
 * @author dchernov
 */
@Module(includes = CiceroneModule.class)
public class AuthNavigationModule {

    @Provides
    @PerAuth
    SignInRouter provideLoginRouter(Lazy<AuthRouterImpl> authRouterLazy) {
        return authRouterLazy.get();
    }

    @Provides
    @PerAuth
    SignUpRouter provideSignUpRouter(Lazy<AuthRouterImpl> authRouterLazy) {
        return authRouterLazy.get();
    }

    @Provides
    @PerAuth
    AuthRouter provideAuthRouter(Lazy<AuthRouterImpl> authRouterLazy) {
        return authRouterLazy.get();
    }

    @Provides
    @PerAuth
    AuthRouterImpl provideAuthRouterImpl(Router router) {
        return new AuthRouterImpl(router);
    }

}
