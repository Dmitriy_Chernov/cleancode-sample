package com.example.dchernov.cleancodesample.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.dchernov.cleancodesample.data.db.models.User;

/**
 * @author dchernov
 */
@Dao
public interface UserDao {

    @Query("SELECT * FROM User LIMIT 1")
    User getUser();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertUser(User user);

    @Delete
    int deleteUser(User user);

    @Query("DELETE FROM User")
    int deleteAllUsers();

}
