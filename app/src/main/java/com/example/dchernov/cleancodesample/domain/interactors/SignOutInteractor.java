package com.example.dchernov.cleancodesample.domain.interactors;

import io.reactivex.Completable;

/**
 * @author dchernov
 */
public interface SignOutInteractor {

    Completable signOut();

}
