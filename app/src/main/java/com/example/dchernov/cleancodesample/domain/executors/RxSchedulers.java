package com.example.dchernov.cleancodesample.domain.executors;


import io.reactivex.Scheduler;

/**
 * @author dchernov
 */
public interface RxSchedulers {

    Scheduler getMainThreadScheduler();
    Scheduler getIOScheduler();

}
