package com.example.dchernov.cleancodesample.di.splash;

import com.example.dchernov.cleancodesample.presentation.splash.view.SplashActivity;

import dagger.Subcomponent;

/**
 * @author dchernov
 */
@PerSplash
@Subcomponent(modules = {SplashModule.class, SplashNavigationModule.class})
public interface SplashComponent {

    @Subcomponent.Builder
    interface Builder {
        SplashComponent build();
    }

    void inject(SplashActivity splashActivity);

}
