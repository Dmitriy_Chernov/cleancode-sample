package com.example.dchernov.cleancodesample.di.splash;

import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.interactors.GetUserInteractor;
import com.example.dchernov.cleancodesample.domain.interactors.GetUserInteractorImpl;

import dagger.Module;
import dagger.Provides;

/**
 * @author dchernov
 */
@Module
public class SplashModule {

    @Provides
    @PerSplash
    GetUserInteractor provideGetUserInteractor(UserRepository userRepository,
                                               RxSchedulers rxSchedulers) {
        return new GetUserInteractorImpl(userRepository, rxSchedulers);
    }

}
