package com.example.dchernov.cleancodesample.domain.executors;

import android.support.annotation.VisibleForTesting;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * @author dmitriy
 */
@VisibleForTesting
public class RxSchedulersTest implements RxSchedulers {

    public RxSchedulersTest() {}

    @Override
    public Scheduler getMainThreadScheduler() {
        return Schedulers.trampoline();
    }

    @Override
    public Scheduler getIOScheduler() {
        return Schedulers.trampoline();
    }
}
