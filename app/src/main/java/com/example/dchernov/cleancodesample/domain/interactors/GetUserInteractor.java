package com.example.dchernov.cleancodesample.domain.interactors;

import com.example.dchernov.cleancodesample.data.db.models.User;

import io.reactivex.Single;

/**
 * @author dchernov
 */
public interface GetUserInteractor {

    Single<User> getUser();

}
