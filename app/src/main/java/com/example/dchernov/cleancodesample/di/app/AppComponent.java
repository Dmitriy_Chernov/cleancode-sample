package com.example.dchernov.cleancodesample.di.app;

import com.example.dchernov.cleancodesample.di.auth.AuthComponent;
import com.example.dchernov.cleancodesample.di.repositories.RepositoriesComponent;
import com.example.dchernov.cleancodesample.di.splash.SplashComponent;

import dagger.Component;

/**
 * @author dchernov
 */
@PerApplication
@Component(modules = {ContextModule.class, DbModule.class, ApiModule.class, SchedulersModule.class})
public interface AppComponent {

    SplashComponent.Builder splashComponentBuilder();
    AuthComponent.Builder authComponentBuilder();
    RepositoriesComponent.Builder repositoriesBuilder();

}
