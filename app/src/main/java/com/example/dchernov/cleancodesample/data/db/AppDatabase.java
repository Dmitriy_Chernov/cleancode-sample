package com.example.dchernov.cleancodesample.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.data.db.models.User;

import static com.example.dchernov.cleancodesample.data.db.AppDatabase.DB_VERSION;

/**
 * @author dchernov
 */
@Database(entities = {User.class, Repository.class}, version = DB_VERSION)
public abstract class AppDatabase extends RoomDatabase {

    static final int DB_VERSION = 1;
    private static final String DB_NAME = "cleancode-sample.db";

    public abstract UserDao getUserDao();
    public abstract RepositoryDao getRepositoryDao();

    public static AppDatabase build(Context context) {
        return Room
                .databaseBuilder(context, AppDatabase.class, DB_NAME)
                .build();
    }

}
