package com.example.dchernov.cleancodesample.di.splash;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * @author dchernov
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerSplash {
}
