package com.example.dchernov.cleancodesample.data.net.models.responses;

import com.google.gson.annotations.SerializedName;

/**
 * @author dchernov
 */
public class RepositoryResponse {

    private long id;

    private String name;

    @SerializedName("description")
    private String desc;

    private String url;

    public RepositoryResponse() {}

    public RepositoryResponse(long id, String name, String desc, String url) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
