package com.example.dchernov.cleancodesample.presentation.splash.presenter;

/**
 * @author dchernov
 */
public interface SplashPresenter {

    void checkAuth();

    void clickBack();

}
