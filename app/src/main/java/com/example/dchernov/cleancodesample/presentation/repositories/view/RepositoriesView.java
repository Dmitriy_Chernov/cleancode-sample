package com.example.dchernov.cleancodesample.presentation.repositories.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.dchernov.cleancodesample.data.db.models.Repository;

import java.util.List;

/**
 * @author dchernov
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface RepositoriesView extends MvpView {

    void showRepositoriesProgress();

    void hideRepositoriesProgress();

    @StateStrategyType(AddToEndStrategy.class)
    void addRepositories(List<Repository> repositoryList);

    void clearRepositories();

    @StateStrategyType(SkipStrategy.class)
    void showRepositoriesUnknownError(String errorText);

}
