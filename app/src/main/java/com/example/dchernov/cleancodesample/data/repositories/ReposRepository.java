package com.example.dchernov.cleancodesample.data.repositories;

import com.example.dchernov.cleancodesample.data.db.models.Repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author dchernov
 */
public interface ReposRepository {

    Single<List<Repository>> getRepositoryList();

    Completable updateRepository(Repository repository);

    Single<List<Long>> insertRepositoryList(List<Repository> repositoryList);

    Completable deleteRepositories();

    Completable deleteRepositories(List<Repository> repositoryList);

}
