package com.example.dchernov.cleancodesample.presentation.repositories.navigation;

import com.example.dchernov.cleancodesample.data.db.models.Repository;

/**
 * @author dchernov
 */
public interface RepositoriesRouter {

    void moveToRepositoryDetails(Repository repository);

    void moveBack();

}
