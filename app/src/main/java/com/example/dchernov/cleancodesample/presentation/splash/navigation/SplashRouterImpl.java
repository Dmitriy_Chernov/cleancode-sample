package com.example.dchernov.cleancodesample.presentation.splash.navigation;

import android.support.annotation.NonNull;

import ru.terrakok.cicerone.Router;

import static com.example.dchernov.cleancodesample.di.NavigationRoutes.AUTH_SCREEN;
import static com.example.dchernov.cleancodesample.di.NavigationRoutes.REPOSITORIES_SCREEN;

/**
 * @author dchernov
 */
public class SplashRouterImpl implements SplashRouter {

    private Router router;

    public SplashRouterImpl(@NonNull Router router) {
        this.router = router;
    }

    @Override
    public void moveBack() {
        router.exit();
    }

    @Override
    public void moveToAuth() {
        router.replaceScreen(AUTH_SCREEN);
    }

    @Override
    public void moveToRepositories() {
        router.replaceScreen(REPOSITORIES_SCREEN);
    }

}
