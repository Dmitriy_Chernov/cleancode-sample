package com.example.dchernov.cleancodesample.data.net.models.responses;

import com.google.gson.annotations.SerializedName;

/**
 * @author dchernov
 */
public class UserResponse {

    private long id;

    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public UserResponse() {}

    public UserResponse(long id, String login, String avatarUrl) {
        this.id = id;
        this.login = login;
        this.avatarUrl = avatarUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return login;
    }

    public void setName(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}