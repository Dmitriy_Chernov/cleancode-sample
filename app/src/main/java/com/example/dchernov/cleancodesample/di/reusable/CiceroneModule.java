package com.example.dchernov.cleancodesample.di.reusable;

import dagger.Module;
import dagger.Provides;
import dagger.Reusable;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * @author dchernov
 */
@Module
public class CiceroneModule {

    private Cicerone<Router> cicerone;

    public CiceroneModule() {
        this.cicerone = Cicerone.create();
    }

    @Provides
    @Reusable
    Router provideRouter() {
        return cicerone.getRouter();
    }

    @Provides
    @Reusable
    NavigatorHolder provideNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

}
