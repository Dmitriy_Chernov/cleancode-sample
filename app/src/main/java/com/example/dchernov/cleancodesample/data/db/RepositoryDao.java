package com.example.dchernov.cleancodesample.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.dchernov.cleancodesample.data.db.models.Repository;

import java.util.List;

/**
 * @author dchernov
 */
@Dao
public interface RepositoryDao {

    @Query("SELECT * FROM Repository ORDER BY name COLLATE NOCASE ASC")
    List<Repository> getRepositoryListOrderByName();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateRepository(Repository repository);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertRepositoryList(List<Repository> repositoryList);

    @Delete
    int deleteRepositories(List<Repository> repositoryList);

    @Query("DELETE FROM Repository")
    int deleteAllRepositories();

}
