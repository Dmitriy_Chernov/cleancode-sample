package com.example.dchernov.cleancodesample.presentation.auth.signup.presenter;

/**
 * @author dchernov
 */
public interface SignUpPresenter {

    void clickSignUp();
    void moveBack();

}
