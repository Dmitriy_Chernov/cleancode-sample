package com.example.dchernov.cleancodesample.presentation.auth.signin.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author dchernov
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface SignInView extends MvpView {

    void showSignInProgress(boolean isShow);

    void showEmailError(String errorText);
    void showPasswordError(String errorText);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showUnknownError(String errorText);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showSignInSuccess();

    void setEnableSignInButton(boolean isEnabled);

}
