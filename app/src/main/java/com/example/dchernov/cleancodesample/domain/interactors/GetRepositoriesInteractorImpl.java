package com.example.dchernov.cleancodesample.domain.interactors;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.data.db.converters.RepositoryConverter;
import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.data.net.GithubApi;
import com.example.dchernov.cleancodesample.data.repositories.ReposRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * @author dchernov
 */
public class GetRepositoriesInteractorImpl implements GetRepositoriesInteractor {

    private static final int START_PAGE = 1;

    private volatile int page = START_PAGE;

    private volatile boolean localAreLoaded = false;
    private volatile boolean allIsLoaded = false;
    private volatile boolean isInLoading = false;

    private RxSchedulers rxSchedulers;
    private GithubApi githubApi;
    private ReposRepository reposRepository;
    private RepositoryConverter repositoryConverter;
    private String userName;
    private int pageSize;

    private PageLoader<List<Repository>> pageLoader;

    private Disposable disposable;

    public GetRepositoriesInteractorImpl(ReposRepository reposRepository,
                                         GithubApi githubApi,
                                         RxSchedulers rxSchedulers,
                                         RepositoryConverter repositoryConverter,
                                         String userName,
                                         int pageSize) {
        this.reposRepository = reposRepository;
        this.githubApi = githubApi;
        this.rxSchedulers = rxSchedulers;
        this.repositoryConverter = repositoryConverter;
        this.userName = userName;
        this.pageSize = pageSize;
    }

    @Override
    public void setPageLoader(@NonNull PageLoader<List<Repository>> pageLoader) {
        this.pageLoader = pageLoader;
    }

    @Override
    public void refresh() {

        if (disposable != null) {
            disposable.dispose();
        }

        allIsLoaded = false;
        isInLoading = false;
        page = START_PAGE;

        if (localAreLoaded) {
            loadNextPage();
        } else {
            loadLocalData();
        }

    }

    @Override
    public void clear() {
        if (disposable != null) {
            disposable.dispose();
        }
        this.pageLoader = null;
    }

    private void loadLocalData() {

        isInLoading = true;

        disposable = reposRepository.getRepositoryList()
                .subscribeOn(rxSchedulers.getIOScheduler())
                .doOnEvent((repositoryList, throwable) -> isInLoading = false)
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe(
                        repositoryList -> {
                            localAreLoaded = true;

                            if (!repositoryList.isEmpty()) {
                                pageLoader.onClear();
                                pageLoader.onGetNextPage(repositoryList);
                            }
                            loadNextPage();
                        },
                        this::handleRepositoriesFailure
                );
    }

    @Override
    public void loadNextPage() {
        if (allIsLoaded || isInLoading) {
            return;
        }

        isInLoading = true;

        disposable = githubApi.getRepositoriesPage(userName, page, pageSize)
                .subscribeOn(rxSchedulers.getIOScheduler())
                .map(repositoryResponseList -> repositoryConverter.convert(repositoryResponseList))
                .doOnEvent((repositoryList, throwable) -> isInLoading = false)
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe(
                        this::handleLoadedRepositoriesSuccess,
                        this::handleRepositoriesFailure
                );
    }

    private void handleLoadedRepositoriesSuccess(@NonNull List<Repository> repositoryList) {
        if (START_PAGE == page++) {
            onFirstPageLoading();
        }
        pageLoader.onGetNextPage(repositoryList);
        saveListToRepository(repositoryList);

        if (repositoryList.size() < pageSize) {
            stopPageLoading();
        }
    }

    private void handleRepositoriesFailure(@NonNull Throwable throwable) {
        pageLoader.onThrowable(throwable);
        stopPageLoading();
    }

    private void onFirstPageLoading() {
        //update UI
        pageLoader.onClear();
        pageLoader.showPageProgress();

        //delete local repositoryList
        reposRepository.deleteRepositories()
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe();
    }

    private void saveListToRepository(@NonNull List<Repository> repositoryList) {
        reposRepository.insertRepositoryList(repositoryList)
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe();
    }

    private void stopPageLoading() {
        allIsLoaded = true;
        pageLoader.hidePageProgress();
    }

}
