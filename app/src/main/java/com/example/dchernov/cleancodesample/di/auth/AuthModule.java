package com.example.dchernov.cleancodesample.di.auth;

import android.content.Context;

import com.example.dchernov.cleancodesample.data.db.converters.UserConverter;
import com.example.dchernov.cleancodesample.data.net.GithubApi;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.auth.validation.LocalAuthValidation;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.interactors.SignInInteractor;
import com.example.dchernov.cleancodesample.domain.interactors.SignInInteractorImpl;
import com.example.dchernov.cleancodesample.presentation.auth.signin.navigation.SignInRouter;

import dagger.Module;
import dagger.Provides;

/**
 * @author dchernov
 */
@Module
public class AuthModule {

    @Provides
    @PerAuth
    SignInInteractor provideSignInInteractor(GithubApi githubApi,
                                           UserRepository userRepository,
                                           UserConverter userConverter,
                                           LocalAuthValidation localAuthValidation,
                                           RxSchedulers rxSchedulers) {
        return new SignInInteractorImpl(githubApi, userRepository, userConverter, localAuthValidation, rxSchedulers);
    }

    @Provides
    @PerAuth
    LocalAuthValidation provideLocalAuthValidation(Context context) {
        return new LocalAuthValidation(context);
    }

    @Provides
    @PerAuth
    UserConverter provideUserConverter() {
        return new UserConverter();
    }

}
