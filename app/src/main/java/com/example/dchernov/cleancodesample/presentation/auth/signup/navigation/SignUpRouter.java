package com.example.dchernov.cleancodesample.presentation.auth.signup.navigation;

/**
 * @author dchernov
 */
public interface SignUpRouter {

    void confirmRegistration();
    void moveBack();

}
