package com.example.dchernov.cleancodesample.presentation.auth.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.di.ComponentStorage;
import com.example.dchernov.cleancodesample.presentation.BackPressedListener;
import com.example.dchernov.cleancodesample.presentation.auth.navigation.AuthRouter;
import com.example.dchernov.cleancodesample.presentation.auth.presenter.AuthPresenterImpl;
import com.example.dchernov.cleancodesample.presentation.auth.signin.view.SignInFragment;
import com.example.dchernov.cleancodesample.presentation.auth.signup.view.SignUpFragment;
import com.example.dchernov.cleancodesample.presentation.repositories.view.RepositoriesActivity;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;

import static com.example.dchernov.cleancodesample.di.NavigationRoutes.REPOSITORIES_SCREEN;
import static com.example.dchernov.cleancodesample.di.NavigationRoutes.SIGN_IN_SCREEN;
import static com.example.dchernov.cleancodesample.di.NavigationRoutes.SIGN_UP_SCREEN;

/**
 * @author dchernov
 */
public class AuthActivity extends MvpAppCompatActivity implements AuthView {

    @Inject
    NavigatorHolder navigatorHolder;

    @Inject
    AuthRouter authRouter;

    @InjectPresenter
    AuthPresenterImpl authPresenter;

    @ProvidePresenter
    AuthPresenterImpl provideAuthPresenter() {
        return new AuthPresenterImpl(authRouter);
    }

    private Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.auth_container) {

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            Fragment fragment = null;
            switch (screenKey) {
                case SIGN_IN_SCREEN:
                    fragment = SignInFragment.newInstance();
                    break;
                case SIGN_UP_SCREEN:
                    fragment = SignUpFragment.newInstance();
                    break;
                default:
                    break;
            }
            return fragment;
        }

        @Override
        public void applyCommand(Command command) {
            if (command instanceof Forward) {
                Forward forward = (Forward) command;

                if (forward.getScreenKey().equals(REPOSITORIES_SCREEN)) {
                    Intent loginIntent = new Intent(AuthActivity.this, RepositoriesActivity.class);
                    startActivity(loginIntent);
                    finish();
                    return;
                }

            }
            super.applyCommand(command);
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(AuthActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ComponentStorage.getInstance().getAuthComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            ComponentStorage.getInstance().clearAuthComponent();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.auth_container);
        if (fragment != null
                && fragment instanceof BackPressedListener
                && ((BackPressedListener) fragment).onBackPressed()) {
            return;
        } else {
            super.onBackPressed();
        }
    }
}
