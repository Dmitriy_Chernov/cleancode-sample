package com.example.dchernov.cleancodesample.di.auth;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author dchernov
 */
@Scope
@Retention(RUNTIME)
public @interface PerAuth {
}
