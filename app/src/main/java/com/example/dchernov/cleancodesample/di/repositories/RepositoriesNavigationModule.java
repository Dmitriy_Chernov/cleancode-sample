package com.example.dchernov.cleancodesample.di.repositories;

import com.example.dchernov.cleancodesample.di.reusable.CiceroneModule;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.RepositoriesRouter;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.RepositoriesRouterImpl;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.SignOutRouter;

import dagger.Lazy;
import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Router;

/**
 * @author dchernov
 */
@Module(includes = CiceroneModule.class)
public class RepositoriesNavigationModule {

    @Provides
    @PerRepositories
    SignOutRouter provideSignOutRouter(Lazy<RepositoriesRouterImpl> repositoriesRouterLazy) {
        return repositoriesRouterLazy.get();
    }

    @Provides
    @PerRepositories
    RepositoriesRouter provideRepositoresRouter(Lazy<RepositoriesRouterImpl> repositoriesRouterLazy) {
        return repositoriesRouterLazy.get();
    }

    @Provides
    @PerRepositories
    RepositoriesRouterImpl provideRepositoresRouterImpl(Router router) {
        return new RepositoriesRouterImpl(router);
    }

}
