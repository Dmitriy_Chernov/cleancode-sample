package com.example.dchernov.cleancodesample.di.repositories;

import android.content.Context;

import com.example.dchernov.cleancodesample.data.db.converters.RepositoryConverter;
import com.example.dchernov.cleancodesample.data.net.GithubApi;
import com.example.dchernov.cleancodesample.data.repositories.ReposRepository;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.interactors.GetRepositoriesInteractor;
import com.example.dchernov.cleancodesample.domain.interactors.GetRepositoriesInteractorImpl;
import com.example.dchernov.cleancodesample.domain.interactors.SignOutInteractor;
import com.example.dchernov.cleancodesample.domain.interactors.SignOutInteractorImpl;
import com.example.dchernov.cleancodesample.presentation.repositories.adapter.RepositoriesAdapter;

import dagger.Module;
import dagger.Provides;

/**
 * @author dchernov
 */
@Module
public class RepositoriesModule {

    @Provides
    @PerRepositories
    SignOutInteractor provideSignOutInteractor(RxSchedulers rxSchedulers,
                                               UserRepository userRepository,
                                               ReposRepository reposRepository) {
        return new SignOutInteractorImpl(rxSchedulers, userRepository, reposRepository);
    }

    @Provides
    @PerRepositories
    GetRepositoriesInteractor provideGetRepositoriesInteractor(ReposRepository reposRepository,
                                                               GithubApi githubApi,
                                                               RxSchedulers rxSchedulers,
                                                               RepositoryConverter repositoryConverter) {

        return new GetRepositoriesInteractorImpl(reposRepository, githubApi, rxSchedulers,
                repositoryConverter, "JakeWharton", GithubApi.PAGE_SIZE);
    }

    @Provides
    @PerRepositories
    RepositoryConverter provideRepositoryConverter() {
        return new RepositoryConverter();
    }

    @Provides
    @PerRepositories
    RepositoriesAdapter provideRepositoriesAdapter(Context context) {
        return new RepositoriesAdapter(context);
    }

}
