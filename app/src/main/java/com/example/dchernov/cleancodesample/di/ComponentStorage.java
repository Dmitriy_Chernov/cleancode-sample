package com.example.dchernov.cleancodesample.di;

import android.app.Application;
import android.util.Log;

import com.example.dchernov.cleancodesample.di.app.AppComponent;
import com.example.dchernov.cleancodesample.di.app.ContextModule;
import com.example.dchernov.cleancodesample.di.app.DaggerAppComponent;
import com.example.dchernov.cleancodesample.di.auth.AuthComponent;
import com.example.dchernov.cleancodesample.di.repositories.RepositoriesComponent;
import com.example.dchernov.cleancodesample.di.splash.SplashComponent;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

/**
 * @author dchernov
 */
public class ComponentStorage {

    private static volatile ComponentStorage instance;

    private volatile AppComponent appComponent;
    private volatile SplashComponent splashComponent;
    private volatile AuthComponent authComponent;
    private volatile RepositoriesComponent repositoriesComponent;

    private Completable injectCompletable;

    public static ComponentStorage getInstance() {
        if (instance == null) {
            synchronized (ComponentStorage.class) {
                if (instance == null) {
                    instance = new ComponentStorage();
                }
            }
        }
        return instance;
    }

    private ComponentStorage() {}


    /*
    App component methods
     */

    public void initAppComponent(Application application) {
        if (appComponent == null) {
            synchronized (this) {
                if (appComponent == null) {
                    appComponent = DaggerAppComponent.builder()
                            .contextModule(new ContextModule(application))
                            .build();
                }
            }
        }
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }


    /*
    Splash component methods
     */

    public SplashComponent getSplashComponent() {
        if (splashComponent == null) {
            synchronized (this) {
                if (splashComponent == null) {
                    splashComponent = getAppComponent().splashComponentBuilder().build();
                }
            }
        }
        return splashComponent;
    }

    public void clearSplashComponent() {
        splashComponent = null;
    }


    /*
    Auth component methods
     */

    public AuthComponent getAuthComponent() {
        if (authComponent == null) {
            synchronized (this) {
                if (authComponent == null) {
                    authComponent = getAppComponent().authComponentBuilder().build();
                }
            }
        }
        return authComponent;
    }

    public void clearAuthComponent() {
        authComponent = null;
    }


    /*
    Repositories component methods
     */

    public RepositoriesComponent getRepositoriesComponent() {
        if (repositoriesComponent == null) {
            synchronized (this) {
                if (repositoriesComponent == null) {
                    repositoriesComponent = getAppComponent().repositoriesBuilder().build();
                }
            }
        }
        return repositoriesComponent;
    }

    public void clearRepositoriesComponent() {
        repositoriesComponent = null;
    }

    public Completable initAppComponentLazy(Application application) {

        if (injectCompletable == null) {
            synchronized (this) {
                if (injectCompletable == null) {

                    Action injectAction = () -> {
                        Log.d("ComponentStorage", "initAppComponentLazy: start");
                        initAppComponent(application);
                        Thread.sleep(3000);
                        Log.d("ComponentStorage", "initAppComponentLazy: end");
                    };

                    injectCompletable = Completable.fromAction(injectAction)
                            .cache()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread());

                    injectCompletable.subscribe();
                }
            }
        }
        return injectCompletable;
    }

}
