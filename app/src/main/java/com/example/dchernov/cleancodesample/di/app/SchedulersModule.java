package com.example.dchernov.cleancodesample.di.app;

import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulersImpl;

import dagger.Module;
import dagger.Provides;

/**
 * @author dchernov
 */
@Module
public class SchedulersModule {

    @Provides
    @PerApplication
    public RxSchedulers provideRxSchedulers() {
        return new RxSchedulersImpl();
    }

}
