package com.example.dchernov.cleancodesample.presentation.splash.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.arellomobile.mvp.MvpPresenter;
import com.example.dchernov.cleancodesample.data.db.models.User;
import com.example.dchernov.cleancodesample.domain.interactors.GetUserInteractor;
import com.example.dchernov.cleancodesample.presentation.BasePresenter;
import com.example.dchernov.cleancodesample.presentation.splash.navigation.SplashRouter;
import com.example.dchernov.cleancodesample.presentation.splash.view.SplashView;

import io.reactivex.functions.Action;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;

/**
 * @author dchernov
 */
public class SplashPresenterImpl extends BasePresenter<SplashView> implements SplashPresenter {

    @Nullable
    private SplashRouter splashRouter;
    @Nullable
    private GetUserInteractor getUserInteractor;

    public SplashPresenterImpl() {

    }

    public void setData(@NonNull SplashRouter splashRouter,
                        @NonNull GetUserInteractor getUserInteractor) {
        this.splashRouter = splashRouter;
        this.getUserInteractor = getUserInteractor;
    }

    @Override
    public void clickBack() {
        if (splashRouter == null) {
            return;
        }
        splashRouter.moveBack();
    }

    @Override
    public void checkAuth() {
        if (getUserInteractor == null) {
            return;
        }
        getUserInteractor.getUser()
                .subscribe(this::handleUserAvailability, this::handleUserFailure);
    }

    private void handleUserAvailability(User user) {
       if (splashRouter == null) {
           return;
       }
       splashRouter.moveToRepositories();
    }

    private void handleUserFailure(Throwable throwable) {
        if (splashRouter == null) {
            return;
        }
        splashRouter.moveToAuth();
    }

}
