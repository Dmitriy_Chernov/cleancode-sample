package com.example.dchernov.cleancodesample.presentation.repositories.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.di.ComponentStorage;
import com.example.dchernov.cleancodesample.di.NavigationRoutes;
import com.example.dchernov.cleancodesample.domain.interactors.GetRepositoriesInteractor;
import com.example.dchernov.cleancodesample.domain.interactors.SignOutInteractor;
import com.example.dchernov.cleancodesample.presentation.auth.view.AuthActivity;
import com.example.dchernov.cleancodesample.presentation.repositories.adapter.RepositoriesAdapter;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.RepositoriesRouter;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.SignOutRouter;
import com.example.dchernov.cleancodesample.presentation.repositories.presenter.RepositoriesPresenterImpl;
import com.example.dchernov.cleancodesample.presentation.repositories.presenter.SignOutPresenterImpl;

import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.commands.Back;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;
import ru.terrakok.cicerone.commands.Replace;
import ru.terrakok.cicerone.commands.SystemMessage;

/**
 * @author dchernov
 */
public class RepositoriesActivity extends MvpAppCompatActivity implements RepositoriesView, SignOutView {

    @Inject
    NavigatorHolder navigatorHolder;

    @Inject
    RepositoriesRouter repositoriesRouter;

    @Inject
    SignOutRouter signOutRouter;

    @Inject
    GetRepositoriesInteractor getRepositoriesInteractor;

    @Inject
    SignOutInteractor signOutInteractor;

    @Inject
    RepositoriesAdapter repositoriesAdapter;

    @InjectPresenter
    RepositoriesPresenterImpl repositoriesPresenter;

    @ProvidePresenter
    RepositoriesPresenterImpl provideRepositoriesPresenter() {
        return new RepositoriesPresenterImpl(repositoriesRouter, getRepositoriesInteractor);
    }

    @InjectPresenter
    SignOutPresenterImpl signOutPresenter;

    @ProvidePresenter
    SignOutPresenterImpl provideSignOutPresenter() {
        return new SignOutPresenterImpl(signOutRouter, signOutInteractor);
    }

    private Navigator navigator = new Navigator() {

        @Override
        public void applyCommand(Command command) {
            if (command instanceof Replace) {
                handleReplaceCommand((Replace) command);
            }
            if (command instanceof Forward) {
                handleForwardCommand((Forward) command);
            }
            if (command instanceof SystemMessage) {
                handleSystemMessage((SystemMessage) command);
            }
            if (command instanceof Back) {
                handleBackCommand((Back) command);
            }
        }

        private void handleReplaceCommand(Replace replaceCommand) {
            switch (replaceCommand.getScreenKey()) {
                case NavigationRoutes.AUTH_SCREEN:
                    navigateToAuthScreen();
                    break;
            }
            RepositoriesActivity.this.finish();
        }

        private void handleForwardCommand(Forward forwardCommand) {
            switch (forwardCommand.getScreenKey()) {
                case NavigationRoutes.REPOSITORY_DETAIL_SCREEN:
                    Repository repository = (Repository) forwardCommand.getTransitionData();
                    navigateToRepositoryDetails(repository);
                    break;
            }
        }

        private void handleSystemMessage(SystemMessage systemMessage) {
            showToast(systemMessage.getMessage());
        }

        private void handleBackCommand(Back backCommand) {
            RepositoriesActivity.this.finish();
        }

        private void navigateToAuthScreen() {
            startActivity(new Intent(RepositoriesActivity.this, AuthActivity.class));
        }

        private void navigateToRepositoryDetails(Repository repository) {
            // TODO: implement navigation to repository item screen
            showToast("Here must be navigation to details of "+repository.getName());
        }

    };

    private ProgressBar listProgress;
    private ProgressBar signOutProgress;
    private RecyclerView repositoriesDataView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ComponentStorage.getInstance().getRepositoriesComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        repositoriesAdapter.setRepositoriesPresenter(repositoriesPresenter);

        listProgress = findViewById(R.id.list_progress);
        signOutProgress = findViewById(R.id.sign_out_progress);
        repositoriesDataView = findViewById(R.id.rv_repositories);
        swipeRefreshLayout = findViewById(R.id.swipe_layout);

        swipeRefreshLayout.setOnRefreshListener(() -> repositoriesPresenter.refresh());

        repositoriesDataView.setAdapter(repositoriesAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.repositories_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_home_sign_out:
                signOutPresenter.clickSignOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            ComponentStorage.getInstance().clearRepositoriesComponent();
        }
    }

    @Override
    public void onBackPressed() {
        repositoriesPresenter.clickBack();
    }

    @Override
    public void showRepositoriesProgress() {
        listProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRepositoriesProgress() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        listProgress.setVisibility(View.GONE);
    }

    @Override
    public void addRepositories(List<Repository> repositoryList) {
        repositoriesAdapter.addRepositoryList(repositoryList);
    }

    @Override
    public void clearRepositories() {
        repositoriesAdapter.clearRepositoryList();
    }


    @Override
    public void showSignOutProgress() {
        signOutProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSignOutProgress() {
        signOutProgress.setVisibility(View.GONE);
    }

    @Override
    public void showRepositoriesUnknownError(String errorText) {
        showToast(errorText);
    }

    @Override
    public void showSignOutUnknownError(String errorText) {
        showToast(errorText);
    }

    private void showToast(String messageText) {
        Toast.makeText(RepositoriesActivity.this, messageText, Toast.LENGTH_SHORT).show();
    }

}
