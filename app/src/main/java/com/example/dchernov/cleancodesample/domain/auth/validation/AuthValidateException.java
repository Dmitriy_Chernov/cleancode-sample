package com.example.dchernov.cleancodesample.domain.auth.validation;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.presentation.auth.signin.models.ValidateErrorModel;

import java.util.List;

/**
 * @author dchernov
 */
public class AuthValidateException extends RuntimeException {

    private List<ValidateErrorModel> validateErrorList;

    public AuthValidateException(@NonNull List<ValidateErrorModel> validateErrorList) {
        super();
        this.validateErrorList = validateErrorList;
    }

    @NonNull
    public List<ValidateErrorModel> getValidateErrorList() {
        return validateErrorList;
    }

}
