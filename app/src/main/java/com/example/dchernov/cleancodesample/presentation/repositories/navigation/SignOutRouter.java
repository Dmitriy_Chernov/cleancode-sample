package com.example.dchernov.cleancodesample.presentation.repositories.navigation;

/**
 * @author dchernov
 */
public interface SignOutRouter {

    void signOut();

}
