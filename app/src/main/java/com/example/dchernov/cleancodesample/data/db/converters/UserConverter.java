package com.example.dchernov.cleancodesample.data.db.converters;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.data.db.models.User;
import com.example.dchernov.cleancodesample.data.net.models.responses.UserResponse;

/**
 * @author dchernov
 */
public class UserConverter {

    public UserConverter() {}

    public User convert(@NonNull UserResponse userResponse) {
        return new User(userResponse.getId(), userResponse.getLogin());
    }
}
