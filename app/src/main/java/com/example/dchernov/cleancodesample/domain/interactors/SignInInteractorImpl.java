package com.example.dchernov.cleancodesample.domain.interactors;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.data.db.converters.UserConverter;
import com.example.dchernov.cleancodesample.data.net.GithubApi;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.auth.validation.LocalAuthValidation;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.SignInFullDataModel;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * @author dchernov
 */
public class SignInInteractorImpl implements SignInInteractor {

    private static final String EMPTY_STRING = "";

    private GithubApi githubApi;
    private UserRepository userRepository;
    private UserConverter userConverter;
    private LocalAuthValidation localAuthValidation;

    private RxSchedulers rxSchedulers;

    public SignInInteractorImpl(GithubApi githubApi,
                                UserRepository userRepository,
                                UserConverter userConverter,
                                LocalAuthValidation localAuthValidation,
                                RxSchedulers rxSchedulers) {
        this.githubApi = githubApi;
        this.userRepository = userRepository;
        this.userConverter = userConverter;
        this.localAuthValidation = localAuthValidation;
        this.rxSchedulers = rxSchedulers;
    }

    @Override
    public Completable signIn(@NonNull SignInFullDataModel signInFullDataModel) {

        return localAuthValidation
                .localValidate(signInFullDataModel)
                .flatMap(signInRequestModel -> githubApi.signIn(signInRequestModel.getToken()))
                .map(userResponse -> userConverter.convert(userResponse))
                .flatMapCompletable(user -> userRepository.setUser(user))
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler());
    }

    @Override
    public Observable<Boolean> controlSignInButton(@NonNull Observable<String> emailFieldListener,
                                                   @NonNull Observable<String> passwordFieldListener) {

        return Observable
                .combineLatest(
                        emailFieldListener.startWith(EMPTY_STRING),
                        passwordFieldListener.startWith(EMPTY_STRING),
                        this::signInButtonIsEnabled
                )
                .distinctUntilChanged();
    }

    private boolean signInButtonIsEnabled(@NonNull String email, @NonNull String password) {
        return (!email.isEmpty() && !password.isEmpty());
    }

}
