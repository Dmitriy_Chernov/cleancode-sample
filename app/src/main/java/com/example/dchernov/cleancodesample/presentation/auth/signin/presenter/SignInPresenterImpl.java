package com.example.dchernov.cleancodesample.presentation.auth.signin.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.dchernov.cleancodesample.domain.auth.validation.AuthValidateException;
import com.example.dchernov.cleancodesample.domain.interactors.SignInInteractor;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.SignInFullDataModel;
import com.example.dchernov.cleancodesample.presentation.auth.signin.models.ValidateErrorModel;
import com.example.dchernov.cleancodesample.presentation.auth.signin.navigation.SignInRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signin.view.SignInView;

import io.reactivex.Observable;

/**
 * @author dchernov
 */
@InjectViewState
public class SignInPresenterImpl extends MvpPresenter<SignInView> implements SignInPresenter {

    private SignInInteractor signInInteractor;
    private SignInRouter signInRouter;

    public SignInPresenterImpl(@NonNull SignInInteractor signInInteractor,
                               @NonNull SignInRouter signInRouter) {
        this.signInInteractor = signInInteractor;
        this.signInRouter = signInRouter;
    }

    @Override
    public void listenInputFields(Observable<String> emailFieldListener,
                                  Observable<String> passwordFieldListener) {
        signInInteractor
                .controlSignInButton(emailFieldListener, passwordFieldListener)
                .subscribe(isEnabled -> {
                    Log.d("SignInPresenterImpl", "listenInputFields before currentTimeMillis: "+System.currentTimeMillis());
                    getViewState().setEnableSignInButton(isEnabled);
                    Log.d("SignInPresenterImpl", "listenInputFields after currentTimeMillis: "+System.currentTimeMillis());

                });
    }

    @Override
    public void clickBack() {
        signInRouter.moveBack();
    }

    @Override
    public void clickSignIn(SignInFullDataModel signInFullDataModel) {

        cleanInputErrors();
        getViewState().showSignInProgress(true);

        signInInteractor.signIn(signInFullDataModel)
                .doOnTerminate(() -> getViewState().showSignInProgress(false))
                .subscribe(
                        this::handleSignInSuccess,
                        this::handleSignInFailure
                );
    }

    @Override
    public void clickSignUp() {
        signInRouter.moveToRegistration();
    }

    private void handleSignInSuccess() {
        getViewState().showSignInSuccess();
        signInRouter.showRepositories();
    }

    private void handleSignInFailure(Throwable throwable) {
        if (throwable instanceof AuthValidateException) {
            handleValidateException(((AuthValidateException) throwable));
        } else {
            handleUnknownThrowable(throwable);
        }
    }

    private void handleValidateException(AuthValidateException authValidateException) {
        for (ValidateErrorModel validateErrorModel: authValidateException.getValidateErrorList()) {
            handleValidateError(validateErrorModel);
        }
    }

    private void handleValidateError(ValidateErrorModel validateErrorModel) {
        switch (validateErrorModel.getField()) {
            case EMAIL:
                getViewState().showEmailError(validateErrorModel.getDescription());
                break;
            case PASSWORD:
                getViewState().showPasswordError(validateErrorModel.getDescription());
                break;
        }
    }

    private void handleUnknownThrowable(Throwable throwable) {
        String msg = throwable.getLocalizedMessage();
        getViewState().showUnknownError(msg);
    }

    private void cleanInputErrors() {
        getViewState().showEmailError(null);
        getViewState().showPasswordError(null);
    }

}
