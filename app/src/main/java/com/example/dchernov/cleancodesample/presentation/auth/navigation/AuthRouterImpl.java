package com.example.dchernov.cleancodesample.presentation.auth.navigation;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.presentation.auth.signin.navigation.SignInRouter;
import com.example.dchernov.cleancodesample.presentation.auth.signup.navigation.SignUpRouter;

import ru.terrakok.cicerone.Router;

import static com.example.dchernov.cleancodesample.di.NavigationRoutes.REPOSITORIES_SCREEN;
import static com.example.dchernov.cleancodesample.di.NavigationRoutes.SIGN_IN_SCREEN;
import static com.example.dchernov.cleancodesample.di.NavigationRoutes.SIGN_UP_SCREEN;

/**
 * @author dchernov
 */
public class AuthRouterImpl implements SignInRouter, SignUpRouter, AuthRouter {

    private Router router;

    public AuthRouterImpl(@NonNull Router router) {
        this.router = router;
    }

    @Override
    public void showRepositories() {
        router.navigateTo(REPOSITORIES_SCREEN);
    }

    @Override
    public void confirmRegistration() {
        // TODO: implement navigation to sign up confirmation screen
        router.showSystemMessage("Section is under development");
    }

    @Override
    public void moveBack() {
        router.exit();
    }

    @Override
    public void moveToRegistration() {
        router.navigateTo(SIGN_UP_SCREEN);
    }

    @Override
    public void moveToSignIn() {
        router.newRootScreen(SIGN_IN_SCREEN);
    }
}
