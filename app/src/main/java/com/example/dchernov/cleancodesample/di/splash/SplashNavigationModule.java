package com.example.dchernov.cleancodesample.di.splash;

import com.example.dchernov.cleancodesample.di.reusable.CiceroneModule;
import com.example.dchernov.cleancodesample.presentation.splash.navigation.SplashRouter;
import com.example.dchernov.cleancodesample.presentation.splash.navigation.SplashRouterImpl;

import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Router;

/**
 * @author dchernov
 */
@Module(includes = CiceroneModule.class)
public class SplashNavigationModule {

    @Provides
    @PerSplash
    SplashRouter provideSplashRouter(Router router) {
        return new SplashRouterImpl(router);
    }

}
