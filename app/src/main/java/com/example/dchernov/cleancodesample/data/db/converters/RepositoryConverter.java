package com.example.dchernov.cleancodesample.data.db.converters;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.data.net.models.responses.RepositoryResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author dchernov
 */
public class RepositoryConverter {

    public RepositoryConverter() {}

    public Repository convert(@NonNull RepositoryResponse repositoryResponse) {
        return new Repository(repositoryResponse.getId(), repositoryResponse.getName());
    }

    public List<Repository> convert(@NonNull Collection<RepositoryResponse> repositoryResponseCollection) {
        List<Repository> repositoryList = new ArrayList<>();

        for (RepositoryResponse repositoryResponse : repositoryResponseCollection) {
            repositoryList.add(convert(repositoryResponse));
        }

        return repositoryList;
    }

}
