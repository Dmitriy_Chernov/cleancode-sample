package com.example.dchernov.cleancodesample.presentation.auth.signin.navigation;

/**
 * @author dchernov
 */
public interface SignInRouter {

    void moveToRegistration();
    void showRepositories();
    void moveBack();

}
