package com.example.dchernov.cleancodesample.presentation.auth.navigation;

/**
 * @author dchernov
 */
public interface AuthRouter {

    void moveToSignIn();

}
