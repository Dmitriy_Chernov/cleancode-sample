package com.example.dchernov.cleancodesample.domain.interactors;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.presentation.auth.signin.models.SignInFullDataModel;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * @author dchernov
 */
public interface SignInInteractor {

    Completable signIn(@NonNull SignInFullDataModel signInFullDataModel);

    Observable<Boolean> controlSignInButton(
            @NonNull Observable<String> emailFieldListener,
            @NonNull Observable<String> passwordFieldListener);

}
