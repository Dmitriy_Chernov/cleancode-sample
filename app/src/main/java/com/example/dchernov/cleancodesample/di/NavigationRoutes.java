package com.example.dchernov.cleancodesample.di;

/**
 * @author dchernov
 */
public final class NavigationRoutes {

    private NavigationRoutes() {}

    public static final String
            AUTH_SCREEN = "auth_screen",
            SIGN_IN_SCREEN = "sign_in_screen",
            SIGN_UP_SCREEN = "sign_up_screen",
            REPOSITORIES_SCREEN = "repositories_screen",
            REPOSITORY_DETAIL_SCREEN = "repository_detail_screen";

}
