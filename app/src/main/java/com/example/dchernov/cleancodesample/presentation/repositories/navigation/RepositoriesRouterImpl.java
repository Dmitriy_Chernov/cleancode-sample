package com.example.dchernov.cleancodesample.presentation.repositories.navigation;

import android.support.annotation.NonNull;

import com.example.dchernov.cleancodesample.data.db.models.Repository;
import com.example.dchernov.cleancodesample.di.NavigationRoutes;

import ru.terrakok.cicerone.Router;

/**
 * @author dchernov
 */
public class RepositoriesRouterImpl implements RepositoriesRouter, SignOutRouter {

    private Router router;

    public RepositoriesRouterImpl(@NonNull Router router) {
        this.router = router;
    }

    @Override
    public void moveToRepositoryDetails(Repository repository) {
        router.navigateTo(NavigationRoutes.REPOSITORY_DETAIL_SCREEN, repository);
    }

    @Override
    public void moveBack() {
        router.exit();
    }

    @Override
    public void signOut() {
        router.replaceScreen(NavigationRoutes.AUTH_SCREEN);
    }
}
