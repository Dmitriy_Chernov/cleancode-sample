package com.example.dchernov.cleancodesample.di.repositories;

import com.example.dchernov.cleancodesample.presentation.repositories.view.RepositoriesActivity;

import dagger.Subcomponent;

/**
 * @author dchernov
 */
@PerRepositories
@Subcomponent(modules = {RepositoriesModule.class, RepositoriesNavigationModule.class})
public interface RepositoriesComponent {

    @Subcomponent.Builder
    interface Builder {
        RepositoriesComponent build();
    }

    void inject(RepositoriesActivity repositoriesActivity);

}
