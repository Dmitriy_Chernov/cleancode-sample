package com.example.dchernov.cleancodesample.di.auth;

import com.example.dchernov.cleancodesample.presentation.auth.signin.view.SignInFragment;
import com.example.dchernov.cleancodesample.presentation.auth.signup.view.SignUpFragment;
import com.example.dchernov.cleancodesample.presentation.auth.view.AuthActivity;

import dagger.Subcomponent;

/**
 * @author dchernov
 */
@PerAuth
@Subcomponent(modules = {AuthModule.class, AuthNavigationModule.class})
public interface AuthComponent {

    @Subcomponent.Builder
    interface Builder {
        AuthComponent build();
    }

    void inject(AuthActivity authActivity);
    void inject(SignInFragment signInFragment);
    void inject(SignUpFragment signUpFragment);

}
