package com.example.dchernov.cleancodesample.domain.interactors;

import com.example.dchernov.cleancodesample.data.repositories.ReposRepository;
import com.example.dchernov.cleancodesample.data.repositories.UserRepository;
import com.example.dchernov.cleancodesample.domain.executors.RxSchedulers;

import io.reactivex.Completable;

/**
 * @author dchernov
 */
public class SignOutInteractorImpl implements SignOutInteractor {

    private RxSchedulers rxSchedulers;
    private UserRepository userRepository;
    private ReposRepository reposRepository;

    public SignOutInteractorImpl(RxSchedulers rxSchedulers, UserRepository userRepository, ReposRepository reposRepository) {
        this.rxSchedulers = rxSchedulers;
        this.userRepository = userRepository;
        this.reposRepository = reposRepository;
    }

    @Override
    public Completable signOut() {

        Completable deleteUsers = userRepository.deleteUsers();
        Completable deleteRepositories = reposRepository.deleteRepositories();

        return deleteUsers
                .concatWith(deleteRepositories)
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler());
    }


}
