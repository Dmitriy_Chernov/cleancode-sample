package com.example.dchernov.cleancodesample.data.net.models.requests;

import android.support.annotation.NonNull;

/**
 * @author dchernov
 */
public class SignInRequestModel {

    private String token;

    public SignInRequestModel(@NonNull String token) {
        this.token = token;
    }

    @NonNull
    public String getToken() {
        return token;
    }

}
