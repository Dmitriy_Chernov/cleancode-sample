package com.example.dchernov.cleancodesample.data.net;


import com.example.dchernov.cleancodesample.data.net.models.responses.RepositoryResponse;
import com.example.dchernov.cleancodesample.data.net.models.responses.UserResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * @author dchernov
 */
public interface GithubApi {

    String API_URL = "https://api.github.com";
    Integer PAGE_SIZE = 50;

    @GET("/user")
    Single<UserResponse> signIn(
            @Header("Authorization") String token
    );

    @GET("/users/{userName}/repos")
    Single<List<RepositoryResponse>> getRepositoriesPage(
            @Path("userName") String userName,
            @Query("page") int page,
            @Query("per_page") int pageSize
    );
}
