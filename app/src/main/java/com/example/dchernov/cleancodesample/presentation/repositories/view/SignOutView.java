package com.example.dchernov.cleancodesample.presentation.repositories.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author dchernov
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface SignOutView extends MvpView {

    void showSignOutProgress();
    void hideSignOutProgress();

    @StateStrategyType(SkipStrategy.class)
    void showSignOutUnknownError(String errorText);

}
