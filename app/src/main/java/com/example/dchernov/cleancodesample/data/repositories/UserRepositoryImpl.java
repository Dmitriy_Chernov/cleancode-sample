package com.example.dchernov.cleancodesample.data.repositories;

import com.example.dchernov.cleancodesample.data.db.UserDao;
import com.example.dchernov.cleancodesample.data.db.models.User;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * @author dchernov
 */
public class UserRepositoryImpl implements UserRepository {

    private UserDao userDao;

    public UserRepositoryImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public Single<User> getUser() {
        return Single.fromCallable(() -> userDao.getUser());
    }

    @Override
    public Completable setUser(User user) {
        return Completable.fromCallable(() -> userDao.insertUser(user));

    }

    @Override
    public Completable deleteUsers() {
        return Completable.fromCallable(() -> userDao.deleteAllUsers());
    }

    @Override
    public Completable deleteUser(User user) {
        return Completable.fromCallable(() -> userDao.deleteUser(user));
    }
}
