package com.example.dchernov.cleancodesample.presentation.splash.navigation;

/**
 * @author dchernov
 */
public interface SplashRouter {

    void moveBack();
    void moveToAuth();
    void moveToRepositories();

}
