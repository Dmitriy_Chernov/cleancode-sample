package com.example.dchernov.cleancodesample.presentation.repositories.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.dchernov.cleancodesample.domain.interactors.SignOutInteractor;
import com.example.dchernov.cleancodesample.presentation.BasePresenter;
import com.example.dchernov.cleancodesample.presentation.repositories.navigation.SignOutRouter;
import com.example.dchernov.cleancodesample.presentation.repositories.view.SignOutView;

import io.reactivex.disposables.Disposable;

/**
 * @author dchernov
 */
@InjectViewState
public class SignOutPresenterImpl extends BasePresenter<SignOutView> implements SignOutPresenter {

    private SignOutRouter signOutRouter;
    private SignOutInteractor signOutInteractor;

    public SignOutPresenterImpl(SignOutRouter signOutRouter, SignOutInteractor signOutInteractor) {
        this.signOutRouter = signOutRouter;
        this.signOutInteractor = signOutInteractor;
    }

    @Override
    public void clickSignOut() {

        getViewState().showSignOutProgress();

        Disposable disposable = signOutInteractor.signOut()
                .doOnTerminate(() -> getViewState().hideSignOutProgress())
                .subscribe(this::handleSignOutSuccess, this::handleSignOutFailure);

        unsubscribeOnDestroy(disposable);
    }

    private void handleSignOutSuccess() {
        signOutRouter.signOut();
    }

    private void handleSignOutFailure(Throwable throwable) {
        getViewState().showSignOutUnknownError(throwable.getLocalizedMessage());
    }
}
