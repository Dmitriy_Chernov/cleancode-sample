package com.example.dchernov.cleancodesample.presentation.auth.signin;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.dchernov.cleancodesample.R;
import com.example.dchernov.cleancodesample.presentation.auth.signin.view.SignInFragment;
import com.example.dchernov.cleancodesample.presentation.auth.view.AuthActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * @author dchernov
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SignInTest {

    @Rule
    public ActivityTestRule<AuthActivity> activityTestRule = new ActivityTestRule<>(AuthActivity.class);

    private AuthActivity activity;

    private CountingIdlingResource idlingResource;

    @Before
    public void beforeEachTest() {

        activity = activityTestRule.getActivity();

        SignInFragment fragment = ((SignInFragment)
                activity.getSupportFragmentManager().findFragmentById(R.id.auth_container));

        idlingResource = fragment.getIdlingResource();

        IdlingRegistry.getInstance().register(idlingResource);
    }

    @After
    public void afterEachTest() {
        if (idlingResource != null) {
            IdlingRegistry.getInstance().unregister(idlingResource);
        }
    }

    @Test
    public void signIn_failure() {
        //type text and then press the button
        onView(withId(R.id.et_login)).perform(typeText("no_reply@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.et_password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.btn_sign_in)).perform(click());

        //wait for idlingResource

        //check HTTP 401 error toast
        onView(withText(containsString("401")))
                .inRoot(withDecorView(not(is(activity.getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }

    @Test
    public void signInButtonEnablingTest() {
        //type text
        onView(withId(R.id.et_login)).perform(typeText("no_reply@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.et_password)).perform(typeText("123456"), closeSoftKeyboard());

        //check that button enabled
        onView(withId(R.id.btn_sign_in)).check(matches(isEnabled()));

        //clear text
        onView(withId(R.id.et_login)).perform(clearText(), closeSoftKeyboard());
        onView(withId(R.id.et_password)).perform(clearText(), closeSoftKeyboard());

        //check that button not enabled
        onView(withId(R.id.btn_sign_in)).check(matches(not(isEnabled())));
    }

}
